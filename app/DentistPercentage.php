<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DentistPercentage extends Model
{
    //
    protected $table = 'dentist_percentage';

   /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
   protected $fillable = [
     'dentist_id','plan_id','percentage'
   ];
}
