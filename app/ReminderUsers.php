<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReminderUsers extends Model
{
   protected $table = 'reminder_users';

   /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
      'reminder_id',
      'user_id',
      'from_user_id',
      'status',
   ];

   public function reminder()
   {
      return $this->belongsTo('App\Reminder','reminder_id');
   }
   public function reminderUser()
   {
      return $this->belongsTo('App\User','from_user_id');
   }
   public function user()
   {
      return $this->belongsTo('App\User','user_id');
   }
}
