<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

class Clinic extends Model implements  HasRoleAndPermissionContract
{
    //
   use  HasRoleAndPermission;

   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clinics';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
      protected $fillable = [
         'name', 'email', 'address','telephone','logo'
      ];

   public function address()
   {
      return $this->belongsTo('App\Address','address_id','id');
   }
   public function contact()
   {
      return $this->belongsTo('App\Contact','contact_id','id');
   }


   public function users(){
      return $this->hasMany('App\User','clinic_id','id');
   }
   public function dentists(){
      return $this->hasMany('App\Dentist','clinic_id','id');
   }
   public function receptionists(){
      return $this->hasMany('App\Recepnist','clinic_id','id');
   }

}
