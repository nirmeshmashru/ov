<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DentalPlan extends Model
{
   //

   /**
      * The table associated with the model.
      *
      * @var string
      */
    protected $table = 'dental_plans';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
       protected $fillable = [
         'title',
         'url',
         'phone',
         'alternate_phone',
         'address',
         'number',
         'borough_id',
         'city_id',
         'state_id',
         'cep',

       ];
}
