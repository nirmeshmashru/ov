<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreatmentType extends Model
{
   //
   /**
   * The table associated with the model.
   *
   * @var string
   */
   protected $table = 'treatment_type';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'title','price','tuss_code','dental_plan_id','default_percentage','private_plan','block_price_alteration','observation','alert_message','status'
   ];

   public function speciality()
   {
      return $this->hasMany('App\TreatmentSpeciality','treatment_type_id','id');
   }
   public function treatment_type_clinic()
   {
      return $this->hasMany('App\TreatmentTypeClinic','treatment_type_id','id');
   }
}
