<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pictogram extends Model
{
    //
    //
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'tooth_defects';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
   protected $fillable = [
     'tooth_number',
     'tooth_type',
     'tooth_left',
     'tooth_top',
     'patient_id','appointment_id',
     'description'
   ];
}
