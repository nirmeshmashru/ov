<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscriptionHistory extends Model
{
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'subscription_history';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
   protected $fillable = [
     'user_id',
     'clinic_id',
   ];
}   
