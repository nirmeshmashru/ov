<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Disease extends Model
{
    //
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'diseases'; 
}
