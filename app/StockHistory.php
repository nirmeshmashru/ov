<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockHistory extends Model
{
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'stock_history';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
   protected $fillable = [
     'item_id',
     'action',
     'quantity',
   ];

   public function item()
   {
      return $this->belongsTo('App\Item','item_id');
   }
}
