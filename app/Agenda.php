<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agenda extends Model
{
   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'appointment_settings';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
      protected $fillable = [
         'settings','user_id'
      ];
}
