<?php

namespace App\Http\Controllers;

use Auth;
use App\Agenda;
use App\AppointmentHolidays;
use Illuminate\Http\Request;

class AgendaController extends Controller
{

      public function index()
      {
         $title    = "Configurações da Agenda";
         $subtitle = "Define como melhor visualizar a sua agenda";
         $activeClass = "appointments";

         $holidays = AppointmentHolidays::where('user_id','=',Auth::user()->id)->get();

         $count = Agenda::where('user_id','=',Auth::user()->id)->count();
         if($count > 0){
            $agenda = Agenda::where('user_id','=',Auth::user()->id)->first();
            $agenda = json_decode($agenda->settings);
            return view('agenda.settings',compact('title','subtitle','activeClass','agenda','holidays'));
         }else {
            return view('agenda.settings',compact('title','subtitle','activeClass'));
         }
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
      {
         $input = $request->all();

         $array = [];
         unset($input['_method']);
         unset($input['_token']);
         $array['settings'] = json_encode($input);
         $array['user_id']  = Auth::user()->id;

         $check = Agenda::where('user_id','=',Auth::user()->id)->count();
         if($check > 0){
            $agenda = Agenda::where('user_id','=',Auth::user()->id)->first();
            $agenda->settings = json_encode($input);
            $agenda->user_id  = Auth::user()->id;
            $agenda->save();

            if($agenda->id){
               return response()->json(['status'=>'success','message' => 'Settings Updated!']);
            }else {
               return response()->json(['status'=>'error','message' => 'Some Error Occured!']);
            }
         }else {
            $agenda = Agenda::create($array);
            if($agenda->id){
               return response()->json(['status'=>'success','message' => 'Settings Created!']);
            }else {
               return response()->json(['status'=>'error','message' => 'Some Error Occured!']);
            }
         }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
