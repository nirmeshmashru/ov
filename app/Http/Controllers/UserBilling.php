<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Carbon\Carbon;
use App\SubscriptionHistory;
use Illuminate\Http\Request;

class UserBilling extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
      {
         //
         $title    = "Billing";
         $subtitle = 'Todas informações do seu relacionadas ao seu estoque';
         $activeClass = "billing";

          // getting all roles
         return view('billing.index', compact('title','subtitle','activeClass'));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
      {
         $user = Auth::user();
         $input = $request->all();
         $token = $input['stripeToken'];

         $user = User::find($user->id);
         try {
               $user->newSubscription('main',$input['plan'])->create($token, [
                  'email' => $user->email,
               ]);
               // $user->trial_ends_at = Carbon::now()->lastOfMonth();
               // $user->save();

               return back()->with('success','Subscription is completed.');
         } catch (Exception $e) {
             return back()->with('success',$e->getMessage());
         }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function subscribeuser()
      {
         $title    = "Subscribe";
         $subtitle = 'Todas informações do seu relacionadas ao seu estoque';
         $activeClass = "billing";

          // getting all roles
         return view('billing.subscribe', compact('title','subtitle','activeClass'));
      }
}
