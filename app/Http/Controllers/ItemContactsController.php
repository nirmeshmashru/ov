<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\ItemContact;

class ItemContactsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
      {
         $title    = "Contacts";
         $subtitle = 'Informações detalhadas de todos tratamentos';
         $activeClass = "contacts";
         $user = Auth::user();
         // getting users
         $contacts = ItemContact::all();

         // getting all roles
         return view('stockcontrol.contacts.index', compact('title','subtitle','activeClass','contacts'));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create()
      {
         $title       = "Add New Contact";
         $subtitle    = "Add New Contact by User";
         $activeClass = "contacts";

         return view('stockcontrol.contacts.create', compact('title','subtitle','activeClass'));
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
      {
         $input = $request->all();
         $contact = ItemContact::create($input);
         if($contact->id){
            return response()->json(['status'=>'success','message' => 'Contact Added!']);
         }else {
            return response()->json(['status'=>'error','message' => 'Some Error Occured']);
         }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function edit($id)
      {
         $title    = "Edit Contact";
         $subtitle = 'Informações detalhadas de todos tratamentos';
         $activeClass = "contacts";
         // getting users
         $contact = ItemContact::find($id);
         // getting all roles
         return view('stockcontrol.contacts.edit', compact('title','subtitle','activeClass','contact'));
      }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request, $id)
      {
         $contact = ItemContact::find($id);
         if($contact->id){
            $input = $request->all();
            $contact->fill($input)->save();
            if($contact->id){
               return response()->json(['status'=>'success','message' => 'Contact Updated!']);
            }else {
               return response()->json(['status'=>'error','message' => 'Some Error Occured']);
            }
         }
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function destroy($id)
      {
         $contact  = ItemContact::find($id);
         if($contact->id){
            $contact->delete();
            return response()->json(['status'=>'success','message' => 'Contact Deleted!']);
         }else {
            return response()->json(['status'=>'error','message' => 'Some Error Occured']);
         }
      }
}
