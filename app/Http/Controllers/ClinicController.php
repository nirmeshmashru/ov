<?php

namespace App\Http\Controllers;

use Auth;
use DB;
use App\Clinic;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Validator;
use Redirect;
use Bican\Roles\Models\Permission;
use Illuminate\Support\Facades\Input;
use Ultraware\Roles\Models\Role;


class ClinicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
      {
         $u = Auth::User();
         $user = User::find($u->id);
         if($user->isAdmin() || $user->hasPermission('clinic.index')){
            $title    = "Clinics";
            $subtitle = "View All Clinics";
            $activeClass = 'clinic';

            $clinics = Clinic::all();
            return view('clinic.index',compact('title','subtitle','activeClass','clinics'));
         }else {
            return redirect('/home');
         }
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create()
      {
         $user = Auth::User();
         if($user->isAdmin() || $user->hasPermission('clinic.create')){
            $title    = "Add New Clinic";
            $subtitle = "Fill up the details for clinic.";
            $activeClass = 'clinic';

            return view('clinic.create',compact('title','subtitle','activeClass'));
         }else {
            return redirect('/home');
         }
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
      {
        //
         $validator = Validator::make($request->all(), [
            'name'     => 'required',
            'address'  => 'required',
            'email'    => 'required|email|unique:clinics',
            'telephone'=> 'required',
            'logo'     => 'image',
         ]);

         if ($validator->fails()) {
              return redirect('clinic/create')
                          ->withErrors($validator)
                          ->withInput();
         }else {

            $clinic = Clinic::create([
                'name'      => $request->name,
                'email'     => $request->email,
                'address'   => $request->address,
                'telephone' => $request->telephone
            ]);

            $image = Input::file('logo');

            if($image){
               $url = $this->upload($image,$clinic->id);
               if($url != ''){
                  $c = Clinic::find($clinic->id);
                  $c->logo = $url;
                  $c->save();
               }
            }
            return redirect('clinic/create')
                  ->with('status', 'Clinic Created!');

         }

      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function show($id)
      {
        //
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function edit($id)
      {
        //
      }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
   {
      $clinic = Clinic::findOrFail($id);
      if($clinic->delete()){
         // deleting directory
         $destinationPath = 'uploads/'.$id; // upload path
         if(file_exists('uploads/'.$id)) {
            rmdir('uploads/'.$id);
         }
         echo "success";
      }else {
         echo "error";
      }
   }

    public function upload($file,$id) {
      // getting all of the post data

      $destinationPath = 'uploads/'.$id; // upload path
      if (!file_exists('uploads/'.$id)) {
         mkdir('uploads/'.$id, 0755, true);
      }
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      $file->move($destinationPath, $fileName); // uploading file to given path
      // sending back with message
      return 'uploads/'.$id."/".$fileName;
   }





}
