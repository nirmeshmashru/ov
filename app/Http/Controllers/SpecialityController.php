<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Speciality;

class SpecialityController extends Controller
{
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
      public function index()
      {
         $title    = "Especialidades";
         $subtitle = 'Todas especialidades cadastradas';
         $activeClass = "treatmenttypes";

         // getting specialiities
         $items = Speciality::all();

         // getting all roles
         return view('specialities.index', compact('title','subtitle','activeClass','specialities'));
     }

   /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
      public function create()
      {
         $title       = "Especialidades";
         $subtitle    = "Novo Item";
         $activeClass = "treatmenttypes";

         return view('specialities.create', compact('title','subtitle','activeClass'));
      }

   /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
      public function store(Request $request)
      {
         $input = $request->all();
         $plan = Speciality::create($input);
         if($plan->id){
            $speciality = Speciality::find($plan->id);
            return response()->json(['status'=>'success','message' => 'Especialidade registrada com sucesso!','json' => $speciality ]);
         }else {
            return response()->json(['status'=>'error','message' => 'Ocorreu um erro!']);
         }
      }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
     public function show($id)
     {
        $title    = "Especialidade";
        $subtitle = '';
        $activeClass = "treatmenttypes";

        $speciality =Speciality::find($id);
         // getting all roles
        return view('specialities.show', compact('title','speciality'));
     }

   /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
      public function edit($id)
      {
         $title        = "Especialidade";
         $subtitle     = "Editar informações relacionadas a especialidade";
         $activeClass  = "treatmenttypes";
           //
         $speciality = Speciality::find($id);
         return view('specialities.edit', compact('title','subtitle','speciality','activeClass'));
      }

   /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function update(Request $request, $id)
   {
      //
      $input = $request->all();
      $speciality = Speciality::find($id);
      if($speciality->id){
         $speciality->fill($input)->save();
         $items = Speciality::all();
         return response()->json(['status'=>'success','message' => "Especialidade atualizada com sucesso!",'json' => $items]);
      }else {
         return response()->json(['status'=>'error','message' => "Ocorreu um erro!"]);
      }
   }

   /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
   public function destroy($id)
   {
      $speciality = Speciality::find($id);
      //$dentist = Dentist::findOrFail($id);
      if($speciality->id){
         $speciality->delete();
         $items = Speciality::all();

         return response()->json(['status'=>'success','message' => "Especialidade excluída com sucesoo!",'json' => $items]);
      }else {
         return response()->json(['status'=>'error','message' => "Ocorreu um erro!"]);
      }
   }


   public function get(){
      $items = Speciality::all();
      if($items){
         return response()->json(['status'=>'success','message' => $items]);
      }else {
         return response()->json(['status'=>'error','message' => 'Ocorreu um erro!']);
      }
   }
}
