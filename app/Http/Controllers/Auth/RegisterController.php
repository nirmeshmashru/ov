<?php

namespace App\Http\Controllers\Auth;

use App\User;
use DB;
use Validator;
use App\Clinic;
use App\Dentist;
use App\Address;
use App\Contact;
use App\BodyTypes;
use App\Borough;
use App\City;
use App\State;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
      return Validator::make($data, [
            'name' => 'required|max:120',
            'email' => 'required|email|max:255|unique:users|unique:clinics',
            'password' => 'required|min:6|confirmed',
            'cpf' => 'required',
            'phone_landline' => 'required',
            'whatsapp_number' => 'required',
            'celular_1' => 'required',
            'street_address' => 'required',
            'borough_id' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'number' => 'required',
      ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
      // creating clinic
      $clinic = Clinic::create([
          'name'      => $data['name'],
          'email'     => $data['email'],
      ]);

      // creating clinic user
      $user = User::create([
         'name'       => $data['name'],
         'email'      => $data['email'],
         'clinic_id'  => $clinic->id,
         'password'  => bcrypt($data['password']),
      ]);

      // sorting clinic image
      $image = Input::file('clinic_image');

      if($image){
         $url = $this->upload($image,$clinic->id);
         if($url != ''){
            $c = Clinic::find($clinic->id);
            $c->logo = $url;
            $c->save();
         }
      }

      // attaching role
      DB::table('role_user')->insert([
          ['role_id' => '2', 'user_id' => $user->id]
      ]);

      //adding patient address table entry
      $address = Address::create($data);
      // adding contact table entry
      $address = Contact::create($data);
      if($address){
         $clinic = Clinic::find($clinic->id);
         $clinic->address_id = $address->id;
         $clinic->contact_id = $address->id;
         $clinic->save();
      }

      // adding a new dentist admin user

      $dentistadmin = Dentist::create([
         'first_name' => $data['name'],
         'email'      => $data['email'],
         'clinic_id'  => $clinic->id,
         'user_id'    => $user->id
      ]);

      return $user;
    }


   public function upload($file,$id) {
      // getting all of the post data

      $destinationPath = 'uploads/'.$id; // upload path
      if (!file_exists('uploads/'.$id)) {
         mkdir('uploads/'.$id, 0755, true);
      }
      $extension = $file->getClientOriginalExtension(); // getting image extension
      $fileName = rand(11111,99999).'.'.$extension; // renameing image
      $file->move($destinationPath, $fileName); // uploading file to given path
      // sending back with message
      return 'uploads/'.$id."/".$fileName;
   }

   /**
   * GETTING CITIES FROM THE STATES
   */
   public function getCities($id){
      $cities = City::where('state_id','=',$id)->get();
      return response()->json(['status'=>'success','message' => $cities]);
   }

   /**
   * GETTING Boroughs FROM THE STATES
   */
   public function getBoroughs($id){
      $cities = Borough::where('city_id','=',$id)->get();
      return response()->json(['status'=>'success','message' => $cities]);
   }
}
