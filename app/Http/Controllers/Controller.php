<?php

namespace App\Http\Controllers;

use App\Http\Middleware\PaymentAccessMiddleware;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Auth;

class Controller extends BaseController
{
   use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

   /**
    * Create a new controller instance.
    *
    * @return void
    */
   public function __construct()
   {
      $this->middleware('auth');
      $this->middleware('subscriptions');
      config(['app.timezone' => 'America/Sao_Paulo']);
      date_default_timezone_set(config('app.timezone'));
   }

}
