<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Recepnist;
use App\Clinic;
use Validator;
use App\User;
use App\State;
use App\Address;
use App\Contact;
use App\Borough;
use App\City;

class RecepnistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $title    = "Recepnists";
      $subtitle = 'Informações detalhadas de todos tratamentos';
      $activeClass = "recepnists";
      $user = Auth::user();
      $subtitle = "Informações detalhadas de todos tratamentos";
      // getting users
      $pUsers = array();

      $users = Recepnist::all();
      $user = Auth::user();
      if($user->isdentistadmin()){
         $users = Recepnist::where('clinic_id','=',$user->clinic_id)->get();
      }
      // getting all roles
      return view('recepnists.index', compact('title','subtitle','users','activeClass'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create()
   {
        //
      $title       = "Add New Recepnist";
      $subtitle    = "Add New Recepnist By Dentist Admin";
      $activeClass = "recepnists";

      $states    = State::pluck('title','id');
      $borough   = Borough::pluck('title','id');

      // getting clinics

      $clinics = Clinic::pluck('name','id');
      return view('recepnists.create', compact('title','subtitle','activeClass','clinics','states','borough'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
          'email'     => 'required|email|unique:users',
       ]);
       if ($validator->fails()) {
         return response()->json(['status'=>'error','message' => 'This email is already in use!']);
       }else {
         $input = $request->all();
         $user = Auth::user();

         $input['name'] = $request->first_name." ".$request->last_name;
         $input['clinic_id'] = $user->clinic_id;
          // adding dentist
          $u = User::create([
             'name'      => $input['name'],
             'email'     => $request->email,
             'clinic_id' => $input['clinic_id'],
             'password'  => bcrypt($request->password),
          ]);
          //adding patient address table entry
          $address = Address::create($input);
          // adding contact table entry
          $contact = Contact::create($input);
          $input['contact_id'] = $contact->id;
          $input['address_id'] = $address->id;

          // adding dentist Role
          if($u->id){
             $user = User::find($u->id);
             $user->attachRole('4');
          }

         // adding recepnist entries
         $recepnist = Recepnist::create($input);
         if($recepnist->id){
            return response()->json(['status'=>'success','message' => 'Recepnist Added.']);
         }else {
          return response()->json(['status'=>'error','message' => 'Some Error Occured']);
         }
      }
   }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $title    = "Edit Recepnist";
      $subtitle = 'Informações detalhadas de todos tratamentos';
      $activeClass = "recepnists";
      $user = Auth::user();
      $subtitle = "Informações detalhadas de todos tratamentos";
      // getting users
      $recepnist = Recepnist::find($id);

      $states = State::pluck('title','id');
      $cities = City::pluck('title','id');
      $borough = Borough::pluck('title','id');
      // getting all roles
      return view('recepnists.edit', compact('title','subtitle','dentist','activeClass','recepnist','states','borough','cities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $input = $request->all();
      $user = Auth::user();

      $input['name'] = $request->first_name." ".$request->last_name;

      $recepnist = Recepnist::find($id);
      if($recepnist->id){

         // update user
         $u = User::where('email','=',$recepnist->email)->first();
         $u->fill($input)->save();
         if($request->password != ''){
            $input['password'] = bcrypt($request->password);
            $u->fill($input)->save();
         }

         // update address
         $address = Address::find($recepnist->address_id);
         if($address->id){
            $address->fill($input)->save();
         }

         // update contact
         $contact = Contact::find($recepnist->contact_id);
         if($contact->id){
            $contact->fill($input)->save();
         }
         // adding recepnist entries
         $recepnist->fill($input)->save();

         if($recepnist->id){
            return response()->json(['status'=>'success','message' => 'Recepnist Updated!']);
         }else {
            return response()->json(['status'=>'error','message' => 'Some Error Occured']);
         }
      }
      else {
         return response()->json(['status'=>'error','message' => 'Some Error Occured']);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $recepnist = Recepnist::find($id);
      //$dentist = Dentist::findOrFail($id);
      if($recepnist->id){
         $u = User::where('email','=',$recepnist->email);
         $u->delete();
         $recepnist->delete();
         return response()->json(['status'=>'success','message' => 'Recepnist Deleted!']);
      }else {
         return response()->json(['status'=>'error','message' => 'Some Error Occured!']);
      }
    }
}
