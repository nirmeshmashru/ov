<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\ItemContact;
use App\StockHistory;
use Auth;

class StockControlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
      {
         $title    = "Controle de Estoque";
         $subtitle = 'Todas informações do seu relacionadas ao seu estoque';
         $activeClass = "stockcontrol";
         $user = Auth::user();
         // getting users
         $items = Item::all();

         // getting all roles
         return view('stockcontrol.index', compact('title','subtitle','items','activeClass'));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create()
      {
         $title       = "Cadastrar novo Item";
         $subtitle    = "Preencher informações do cadastro do item";
         $activeClass = "stockcontrol";

         return view('stockcontrol.create', compact('title','subtitle','activeClass'));
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
      {
         $input = $request->all();
         $find = Item::where([['title','=',$input['title']],['clinic_id','=',Auth::user()->clinic_id]])->count();

         if($find > 0) {
            return response()->json(['status'=>'error','message' => "This Item is already used!"]);
         }else {
            $input['clinic_id']  = Auth::user()->clinic_id;
            $input['user_id']    = Auth::user()->id;
            $input['created_by'] = Auth::user()->id;
            $item = Item::create($input);

            if (!file_exists('uploads/'.Auth::user()->clinic_id)) {
               mkdir('uploads/'.Auth::user()->clinic_id, 0755, true);
            }
            if (!file_exists('uploads/'.Auth::user()->clinic_id."/items/")) {
               mkdir('uploads/'.Auth::user()->clinic_id."/items", 0755, true);
            }
            $url = $this->upload($input['image_url'],Auth::user()->clinic_id."/items");
            $item->image_url = $url;
            $item->save();
            if($item->id){
               return response()->json(['status'=>'success','message' => 'Item Adicionado!']);
            }else {
               return response()->json(['status'=>'error','message' => 'Ocorreu Algum Problema!']);
            }
         }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function edit($id)
      {
         $title    = "Cadastro de Item";
         $subtitle = 'Informações do item';
         $activeClass = "stockcontrol";
         // getting users
         $item = Item::find($id);
         // getting all roles
         return view('stockcontrol.edit', compact('title','subtitle','activeClass','item'));
      }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request, $id)
      {
         $item = Item::find($id);
         if($item->id){
            $input = $request->all();


            // deleting item
            // unlink($item->image_url);

            // updating item
            if(isset($input['image_url'])){
               if (!file_exists('uploads/'.Auth::user()->clinic_id)) {
                  mkdir('uploads/'.Auth::user()->clinic_id, 0755, true);
               }
               if (!file_exists('uploads/'.Auth::user()->clinic_id."/items/")) {
                  mkdir('uploads/'.Auth::user()->clinic_id."/items", 0755, true);
               }
               $url = $this->upload($input['image_url'],Auth::user()->clinic_id."/items");
               $item->image_url = $url;
               $item->save();
            }
            unset($input['image_url']);
            $item->fill($input)->save();

            return response()->json(['status'=>'success','message' => 'Item Atualizado!']);
         }else {
            return response()->json(['status'=>'error','message' => 'Ocorreu Algum Problema!']);
         }
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function destroy($id)
      {
         $item  = Item::find($id);
         if($item->id){
            $item->delete();
            return response()->json(['status'=>'success','message' => 'Item Excluído!']);
         }else {
            return response()->json(['status'=>'error','message' => 'Ocorreu Algum Problema!']);
         }
      }

   /**
    * Quote items for the selected contacts
    *
    * @return response
    */
      public function quoteItems(){
         $title    = "Quotação de Compras";
         $subtitle = 'Criar um relatório de quotação customizado';
         $activeClass = "stockcontrol";

         $items    = Item::all();
         $contacts = ItemContact::all();

         return view('stockcontrol.quoteitems', compact('title','subtitle','activeClass','items','contacts'));
      }

   /**
    * Quote items for the selected contacts
    *
    * @return response
    */
      public function updateStock(Request $request,$id){
         $stock = Item::find($id);
         if($stock->id){
            if($request->action == 'add'){
               $stock->quantity = $stock->quantity + $request->quantity;
            }else {
               $stock->quantity = $stock->quantity - $request->quantity;
            }

            // checking stock quantity
            if($stock->quantity > 0){
               $stock->save();

               // updating stock history
               $input            = $request->all();
               $input['item_id'] = $stock->id;
               $stockHistory  = StockHistory::create($input);

               return response()->json(['status'=>'success','message' => 'Estoque Atualizado!']);
            }else {
               return response()->json(['status'=>'error','message' => 'Occoreu Algum Problema!']);
            }
         }else {
            return response()->json(['status'=>'error','message' => 'Occoreu Algum Problema!']);
         }
      }

   /**
    * Quote items for the selected contacts
    *
    * @return response
    */
      public function getItemHistory($id){
         $history = StockHistory::where('item_id','=',$id)->get();
         $count   = StockHistory::where('item_id','=',$id)->count();
         if($count > 0){
            $i = 0;
            foreach($history as $data){
               $history[$i]->item = $data->item;
               $i++;
            }
            return response()->json(['status'=>'success','message' => 'Sucesso','data' => $history]);
         }else {
            return response()->json(['status'=>'error','message' => 'Item Sem Hístorico!']);
         }
      }




    public function upload($file,$id) {
     // getting all of the post data
     $destinationPath = 'uploads/'.$id; // upload path
     if (!file_exists('uploads/'.$id)) {
        mkdir('uploads/'.$id, 0755, true);
     }
     $extension = $file->getClientOriginalExtension(); // getting image extension
     $fileName = rand(11111,99999).'.'.$extension; // renameing image
     $file->move($destinationPath, $fileName); // uploading file to given path
     // sending back with message
     return 'uploads/'.$id."/".$fileName;
   }
}
