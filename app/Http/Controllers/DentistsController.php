<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use App\Dentist;
use Auth;
use App\Clinic;
use Validator;
use Bican\Roles\Models\Permission;
use Illuminate\Support\Facades\Input;
use Ultraware\Roles\Models\Role;

class DentistsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
   {
      //
      $title    = "Dentists";
      $subtitle = 'Informações detalhadas de todos tratamentos';
      $activeClass = "dentists";
      $user = Auth::user();
      $subtitle = "Informações detalhadas de todos tratamentos";
      // getting users
      $pUsers = array();

      $users = Dentist::all();
      $user = Auth::user();
      if($user->hasRole('dentistadmin') || $user->hasRole('receptionist')){
         $users = Dentist::where('clinic_id','=',$user->clinic_id)->get();
      }
      // getting all roles
      return view('dentists.index', compact('title','subtitle','users','activeClass'));
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function create()
   {
      $title       = "Cadastrar Usuário";
      $subtitle    = "Informações do usuário";
      $activeClass = "dentists";

      // getting clinics
      $clinics = Clinic::pluck('name','id');
      return view('dentists.create', compact('title','subtitle','activeClass','clinics'));
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
      public function store(Request $request)
      {
         $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
         ]);

         if ($validator->fails()) {
            return response()->json(['status'=>'error','message' => 'Já existe esse email cadastrado no nosso sistema!']);
         }else {

            // adding dentist
            $u = User::create([
                'name'      => $request->first_name." ".$request->last_name,
                'email'     => $request->email,
                'clinic_id' => $request->clinic_id,
                'password'  => bcrypt($request->password),
            ]);

            // Adding User Role
            if($u->id){
               if($request->role == 'admin'){
                  $request->role = 'dentist';
               }
               if($request->role == 'dentist'){
                  $user = User::find($u->id);
                  $user->attachRole('3');
               }
               if($request->role == 'dentistadmin'){
                  $user = User::find($u->id);
                  $user->attachRole('2');
               }
            }
            $input = $request->all();
            $input['user_id'] = $u->id;

            // Adding dentist entries
            $dentist = Dentist::create($input);

            return response()->json(['status'=>'success','message' => 'Usuário cadastrado com sucesso!']);
         }
      }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function show($id)
      {
         $title       = "Dentist Profile";
         $subtitle    = "Show Dentist Profile";
         $activeClass = "dentists";

         $dentist = Dentist::find($id);
         return view('dentists.show', compact('title','subtitle','activeClass','dentist'));
      }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function edit($id)
      {
         $title    = "Edit Dentist";
         $subtitle = 'Informações detalhadas de todos tratamentos';
         $activeClass = "dentists";
         $user = Auth::user();
         $subtitle = "Informações detalhadas de todos tratamentos";
         // getting users
         $pUsers = array();
         $clinics = Clinic::pluck('name','id');

         $dentist = Dentist::find($id);
         $user = User::where('id','=',$dentist->user_id)->first();
         if($user->hasRole('dentistadmin')){
            $dentist->role = 'dentistadmin';
         }
         if($user->hasRole('dentist')){
            $dentist->role = 'dentist';
         }
         // getting all roles
         return view('dentists.edit', compact('title','subtitle','dentist','activeClass','clinics'));
      }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
      public function update(Request $request, $id)
      {
         //
         $dentist = Dentist::find($id);
         $input = $request->all();
         if($dentist){
            $dentist->fill($input)->save();

            // checking role
            $user = User::where('id','=',$dentist->user_id)->first();
            if($request->role == 'dentistadmin'){
               $user->detachAllRoles();
               $user->attachRole('2');
            }
            if($request->role == 'dentist'){
               $user->detachAllRoles();
               $user->attachRole('3');
            }

            return response()->json(['status'=>'success','message' => 'Dentist Added!']);
         }else {
            return response()->json(['status'=>'error','message' => 'Some Error Occured!']);
         }
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   public function destroy($id)
   {
      $dentist = Dentist::find($id);
      //$dentist = Dentist::findOrFail($id);
      if($dentist->user_id){
         $u = User::findOrFail($dentist->user_id);
         $u->delete();
         $dentist->delete();
         return "success";
      }else {
         return "error";
      }
   }
}
