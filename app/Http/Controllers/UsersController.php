<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\User;
use Auth;
use App\Clinic;
use Validator;
use Ultraware\Roles\Models\Permission;
use App\PermissionRole;
use App\Dentist;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $title = "Users";
        $subtitle = 'Informações detalhadas de todos tratamentos';
        $activeClass = "users";
        $user = Auth::user();
        $subtitle = "Informações detalhadas de todos tratamentos";
        // getting users
        $pUsers = array();

        $users = DB::table('users')
            ->leftjoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->where('role_id', '=', '2')
            ->get();

        // getting all roles
        $roleEntry = DB::table('roles')->get();

        foreach ($users as $data) {
            $user = User::find($data->id);
            $clinic = Clinic::find($data->clinic_id);

            // adding data into external array
            $fusers = new \stdClass();
            $fusers->id = $data->id;
            $fusers->name = $data->name;
            $fusers->email = $data->email;
            $fusers->clinic_name = $clinic->name;
            array_push($pUsers, $fusers);
        }
        return view('users.index', compact('title', 'subtitle', 'pUsers', 'activeClass'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = "Add New Dentist Admin";
        $subtitle = "Add New user by Dentist Admin User";
        $activeClass = "users";

        // getting clinics

        $clinics = Clinic::pluck('name', 'id');
        return view('users.create', compact('title', 'subtitle', 'activeClass', 'clinics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'clinic_id' => 'required',
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('users/create')
                ->withErrors($validator)
                ->withInput();
        } else {

            // adding dentist
            $clinic = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'clinic_id' => $request->clinic_id,
                'password' => bcrypt($request->password),
            ]);

            // adding dentist Role

            $user = User::find($clinic->id);
            $user->attachRole('2');


            return redirect('users/create')
                ->with('status', 'Dentist Admin Created!');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->trashed()) {
            return response()->json(['status' => 'success', 'message' => "User Deleted!"]);
        } else {
            return response()->json(['status' => 'error', 'message' => 'Some Error Occured!']);
        }
    }

    /**
     * Generate profile as per the current logged in user
     *
     */
    public function profile()
    {
        $user = Auth::user();

        if ($user->hasRole('dentistadmin')) {
            $data = $user;
        }
        if ($user->hasRole('dentist')) {
            $data = Dentist::where('user_id', '=', $user->id)->first();
        }
        if ($user->hasRole('receptionist')) {
            $data = Receptionist::where('user_id', '=', $user->id)->first();
        }

        $title = "User Profile";
        $subtitle = 'Informações detalhadas de todos tratamentos';
        $activeClass = "";
        $subtitle = "Informações detalhadas de todos tratamentos";

        return view('users.profile.index', compact('title', 'subtitle', 'data', 'activeClass'));

    }

    /**
     * Generate profile as per the current logged in user
     *
     */
    public function userDetails()
    {
        $user = Clinic::where('id', '=', Auth::user()->clinic_id)->first();
        return $user;
    }

    /**
     * GENERATE INVOICES
     *
     */

    public function invoices()
    {
        $title = "User Invoices";
        $subtitle = "Download All User Invoices";
        $activeClass = "users";

        $user = User::find(Auth::user()->id);
        $invoices = $user->invoices();
        return view('users.invoices', compact('title', 'subtitle', 'invoices', 'activeClass'));
    }


    /**
     * MANAGE ALL USERS AS PER CLINIC
     *
     */

    public function manage()
    {
        $title = "Manage Clinic User";
        $subtitle = "All the clinic User List";
        $activeClass = "users_management";

        // get all users
        $users = User::where([['clinic_id', '=', Auth::user()->clinic_id], ['role_id', '>', '1']])
            ->leftJoin('role_user', 'users.id', '=', 'role_user.user_id')
            ->leftJoin('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.name AS rolename', 'roles.*', 'users.name AS username')
            ->orderBy('roles.slug', 'ASC')
            ->whereNotIn('users.id', array(Auth::user()->id))
            ->get();

        $i = 0;
        foreach ($users as $data) {
            // if($users[$i]->hasRole('dentist')){
            //    $dentist = Dentist::where('user_id','=',$data->id)->first();
            //    $users[$i]->profile_url =$dentist->profile_url;
            // }
            // if($users[$i]->hasRole('patient')){
            //    $patient = Patient::where('user_id','=',$data->id)->first();
            //    $users[$i]->profile_url =$patient->profile_url;
            // }
            // if($users[$i]->hasRole('receptionist')){
            //    $r = Receptionist::where('user_id','=',$data->id)->first();
            //    $users[$i]->profile_url =$r->profile_url;
            // }
            $i++;
        }

        return view('users.manage', compact('title', 'subtitle', 'users', 'activeClass'));
    }


    /**
     * Adding current controller permissions
     * Manually write all controller methods
     *
     * @route /controller/permission
     */

    public function permission()
    {

        $array = array(
            array('Show All Users', 'userscontroller.index', 'User', 'Show All Users'),
            array('Create New User', 'userscontroller.create', 'User', 'Create New User'),
            array('Manage Users', 'userscontroller.manage', 'User', 'Manage Users'),
        );

        // checking
        foreach ($array as $data) {
            $count = Permission::where('slug', '=', $data[1])->count();
            if ($count == '0') {
                // adding permission
                Permission::create([
                    'name' => $data[0],
                    'slug' => $data[1],
                    'model' => $data[2],
                    'description' => $data[3],
                ]);
            }
        }
        exit;
    }

}
