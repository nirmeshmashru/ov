<?php

namespace App\Http\Controllers;

use Ultraware\Roles\Models\Role;
use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{

    public function index()
    {
        $title = "Appointment";
        $subtitle = "Book an Appointment";
        $activeClass = "dashboard";
        $user = Auth::user();
        return view('home', compact('title', 'subtitle', 'activeClass'));
    }

    public function joinus()
    {
        $title = "join us";
        $subtitle = "Book an Appointment";
        $activeClass = "dashboard";
        $user = Auth::user();
        return view('home', compact('title', 'subtitle', 'activeClass'));
    }
}
