<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\DentalPlan;
use App\State;
use App\Borough;
use App\City;
use Ultraware\Roles\Models\Permission;
use App\PermissionRole;

class DentalplansController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
      $title        = "Dental Plans";
      $subtitle     = "Dental Plans List";
      $activeClass  = "dentalplans";
      $plans        =  DentalPlan::all();
      return view('dentalplans.index',compact('title','subtitle','activeClass','plans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function create()
      {
         $title        = "Create a Dental Plan";
         $subtitle     = "Dental Plans List";
         $activeClass  = "dentalplans";
         $states     = State::pluck('title','id');
         $borough    = borough::pluck('title','id');

         return view('dentalplans.create',compact('title','subtitle','activeClass','states','borough'));
      }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input = $request->all();
      $plan = DentalPlan::create($input);
      if($plan->id){
         return response()->json(['status'=>'success','message' => 'Dental Plan Created!']);
      }else {
         return response()->json(['status'=>'error','message' => 'Some Error Occured!']);
      }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
      $title    = "Dental Plan";
      $subtitle = 'Informações detalhadas de todos tratamentos';
      $activeClass = "dentalplans";
      $user = Auth::user();
      $subtitle = "Informações detalhadas de todos tratamentos";
      $plan = DentalPlan::find($id);
      return view('treatments.show', compact('title','subtitle','patient','activeClass','plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $title        = "Update Dental Plan";
      $subtitle     = "Update Plans List";
      $activeClass  = "dentalplans";
        //
      $plan = DentalPlan::find($id);
      $states     = State::pluck('title','id');
      $borough    = borough::pluck('title','id');
      $cities       = City::pluck('title','id');
      return view('dentalplans.edit', compact('title','subtitle','patient','activeClass','plan','states','cities','borough'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
      $input = $request->all();
      $dentalplan = DentalPlan::find($id);
      if($dentalplan->id){
         $dentalplan->fill($input)->save();
         return response()->json(['status'=>'success','message' => "Dental Plan Updated!"]);
      }else {
         return response()->json(['status'=>'error','message' => "Some Error Occured!"]);
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


   /**
    * Adding current controller permissions
    * Manually write all controller methods
    *
    * @route /controller/permission
    */

    public function permission(){

      $array = array(
         array('Show All Dental Plan','dentalplanscontroller.index','DentalPlan','Show All Dental Plan'),
         array('Create Dental Plan','dentalplanscontroller.create','DentalPlan','Create New Dental Plan'),
         array('Store Dental Plan','dentalplanscontroller.store','DentalPlan','Save Dental Plan'),
         array('Show Dental Plan','dentalplanscontroller.show','DentalPlan','Show Dental Plan'),
         array('Edit Dental Plan','dentalplanscontroller.edit','DentalPlan','edit Dental Plan'),
         array('Update Dental Plan','dentalplanscontroller.update','DentalPlan','Update Dental Plan'),
         array('Destroy Dental Plan','dentalplanscontroller.destroy','DentalPlan','Destroy Dental Plan'),
      );

      // checking
      foreach($array as $data){
         $count = Permission::where('slug','=' , $data[1])->count();
         if($count == '0'){
            // adding permission
            Permission::create([
                'name'      => $data[0],
                'slug'      => $data[1],
                'model'     => $data[2],
                'description' => $data[3],
            ]);
         }
      }
      exit;
   }


}
