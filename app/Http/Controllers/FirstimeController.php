<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Speciality;
use Auth;
use App\Dentist;

class FirstimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
      public function index()
      {
         $title    = "Setup Odontovision";
         $subtitle = 'Informações detalhadas de todos tratamentos';
         // getting users

         $specialities = Speciality::pluck('title','id');
         if(Auth::user()->hasRole('dentistadmin')){
            $user = Dentist::where('user_id','=',Auth::user()->id)->first();
         }

         // getting all roles
         return view('firsttime.index', compact('title','subtitle','specialities','user'));
      }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
