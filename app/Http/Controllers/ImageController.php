<?php
namespace App\Http\Controllers;
use App\Http\Requests;
use Guzzle\Tests\Plugin\Redirect;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ImageController extends Controller {

	/**
	 * Show the form for uploading a new resource.
	 *
	 * @return Response
	 */
	public function upload()
	{
		return view('imageupload');
	}

	/**
	 * Store a newly uploaded resource in storage.
	 *
	 * @return Response
	 */
	public function store(){
		// Store records process
   	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function show(){
		// Show lists of the images
    }
}
