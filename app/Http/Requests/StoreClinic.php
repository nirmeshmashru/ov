<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreClinic extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'     => 'required',
           'address'  => 'required',
           'email'    => 'email|unique',
           'telephone'=> 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Precisa informar nome',
            'address.required' => 'Precisa informar endreço',
            'email.email' => 'Formato do email está invalido',
            'email.unique' => 'Já existe esse email no nosso banco de dados',
            'telephone.required'=> 'Precisa informar telefone',
        ];
    }
}
