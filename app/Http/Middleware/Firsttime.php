<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Route;
use Closure;

class Firsttime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
   public function handle($request, Closure $next)
   {
      $route = Route::getCurrentRoute()->getPath();
      if($request->user()->first_time == '1'){
         if($request->method() == 'GET'){
            if($route != 'firsttime'){
               return redirect('firsttime');
            }
         }
      }
      else {
         //return redirect('billing/subscribe');
      }

      return $next($request);
   }
}
