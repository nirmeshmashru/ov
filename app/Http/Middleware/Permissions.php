<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Auth;
use App\Clinic;
use App\User;
use App\Reminder;
use App\ReminderUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class Permissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // finding the number of reminders

        $reminders = ReminderUsers::where([['user_id', '=', $request->user()->id], ['status', '=', '1']])->count();
        if ($reminders == '0') {
            $request->user()->reminderCount = '';
        } else {
            $request->user()->reminderCount = $reminders;
        }

        // array having default permissions

        $dConfig = array('ReminderController');

        // print_r(Route::getCurrentRoute()->getActionName());
        $route = str_replace("@", '.', Route::getCurrentRoute()->getActionName());
        $route = strtolower(stripslashes(str_replace('App\Http\Controllers', '', $route)));

         if($route == 'firstimecontroller.index'){
         }else {
            if (!$request->user()->hasRole('admin')) {
               if (!$request->user()->hasPermission($route)) {
                   abort(404, 'Unauthorized action.');
               }
            }
         }

        return $next($request);
    }
}
