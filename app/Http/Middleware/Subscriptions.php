<?php

namespace App\Http\Middleware;

use Closure;
use DB;
use Auth;
use App\Clinic;
use App\User;
use Illuminate\Support\Facades\Route;

class Subscriptions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
      public function handle($request, Closure $next)
      {
         $route = Route::getCurrentRoute()->getPath();

         $clinic = Clinic::find($request->user()->clinic_id);
         $subscriptions = DB::table('subscriptions')->where('clinic_id', $request->user()->clinic_id)->first();
         if(!empty($subscriptions)){
            $user = User::find($subscriptions->user_id);

            // if user is subscribed
            if ($user && ! $user->subscribed('main')) {
               if($route != 'billing'){
                  return redirect('billing');
               }
            }
         }
         else {
            //return redirect('billing/subscribe');
         }

       return $next($request);
      }
}
