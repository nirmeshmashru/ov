<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    //
    //
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'items';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
   protected $fillable = [
     'title',
     'image_url',
     'quantity',
     'clinic_id',
     'user_id',
     'min_stock',
     'purchased_date',
     'created_by',
   ];

}
