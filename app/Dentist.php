<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dentist extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dentists';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
      'profile_url',
      'first_name',
      'clinic_id',
      'user_id',
      'last_name',
      'email',
      'gender',
      'dob',
      'cpf',
      'rg',
      'observation',
      'cro',
      'honors',
      'resident_in_clinic',
      'use_whatsapp',
      'accept_calls',
      'address',
      'city',
      'state',
      'country',
      'zip'
   ];

   public function user()
   {
       return $this->belongsTo('App\User','user_id');
   }
   public function clinic()
   {
       return $this->belongsTo('App\Clinic');
   }

   public function contact(){
      return $this->belongsTo('App\Contact','contact_id');
   }
   public function address(){
      return $this->belongsTo('App\Address','address_id');
   }
   public function appointments(){
      return $this->hasMany('App\Appointment','dentist_id','user_id');
   }
}
