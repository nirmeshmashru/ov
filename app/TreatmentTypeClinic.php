<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreatmentTypeClinic extends Model
{
   //
   /**
   * The table associated with the model.
   *
   * @var string
   */
   protected $table = 'treatment_type_clinic';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'clinic_id','treatment_type_id','price','observation','alert_message','status','default_percentage','standard_dentist_percentage'
   ];

}
