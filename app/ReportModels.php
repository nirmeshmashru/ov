<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportModels extends Model
{
    //
    protected $table = 'report_models';

   /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
   protected $fillable = [
     'title',
   ];
}
