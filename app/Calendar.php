<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    //

   public function patient()
   {
      return $this->belongsTo('App\Patient','patient_id');
   }
}
