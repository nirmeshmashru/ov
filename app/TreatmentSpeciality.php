<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TreatmentSpeciality extends Model
{
   //
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'treatment_specialities';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
   protected $fillable = [
     'treatment_type_id',
     'speciality_id',
   ];
}
