<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    //
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'patients';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
      'first_name',
      'last_name',
      'clinic_id',
      'user_id',
      'email',
      'gender',
      'DOB',
      'CPF',
      'RG',
      'observation',
      'cro',
      'honors',
      'resident_in_clinic',
      'use_whatsapp',
      'accept_calls',
      'address',
      'professional_id',
      'has_dental_plan',
      'indication',
      'sms_confirmation',
      'allow_profile_use',
      'vip',
      'maritial_status',
      'nationality',
      'body_type_id',
      'take_drugs',
      'has_birth_defect',
      'take_preg_pills',
      'bone_dev_stage',
      'has_prev_surgeries',
      'current_health',
      'wheel_chair',
      'height',
      'weight'
   ];

   public function user()
   {
       return $this->belongsTo('App\User');
   }

   public function clinic()
   {
       return $this->belongsTo('App\Clinic');
   }

   public function appointments(){
      return $this->hasMany('App\Appointment','patient_id','id');
   } 

   public function professional(){
      return $this->belongsTo('App\Dentist','professional_id','id');
   }

   public function contact(){
      return $this->belongsTo('App\Contact','contact_id');
   }
   public function address(){
      return $this->belongsTo('App\Address','address_id');
   }
   public function bodyType(){
      return $this->belongsTo('App\BodyTypes','body_type_id');
   }
   public function speciality(){
      return $this->hasMany('App\PatientSpeciality','patient_id','id');
   }
   public function specialityEdit(){
      return $this->hasMany('App\PatientSpeciality','patient_id','id');
   }
}
