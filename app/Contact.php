<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    //
   protected $table = 'contact';

   protected $fillable = [
      'phone_landline','celular_1','celular_2','whatsapp_number','skype'
   ];
}
