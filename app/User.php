<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Ultraware\Roles\Traits\HasRoleAndPermission;
use Ultraware\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;
use Laravel\Cashier\Billable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable implements  HasRoleAndPermissionContract
{
    use  HasRoleAndPermission;
    use Notifiable;
    use Billable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','clinic_id','first_time'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The roles that belong to the user.
     */
      // public function roles()
      // {
      //   return $this->hasOne('Ultraware\Roles\Models\Role');
      // }

      public function dentist()
      {
         return $this->belongsTo('App\Dentist','id');
      }


      public function clinic(){
         return $this->belongsTo('App\Clinic','clinic_id','id');
      }

      public function clinicAddress(){
         return $this->hasManyThrough('App\Address','App\Clinic','id','id');
      }

}
