<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
   //
   protected $table = 'reminders';

  /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
    'title',
    'user_id',
    'content',
    'reminder_date',
    'status',
   ];

   public function reminderUser()
   {
     return $this->hasMany('App\ReminderUsers','reminder_id');
   }
}
