<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treatment extends Model
{
   //
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'treatments';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
   protected $fillable = [
     'treatment_type_id',
     'patient_id',
     'dentist_id',
     'start_date',
     'appointment_id',
     'payment_id',
     'observation',
     'treatment_type_id',
   ];

   public function payment()
   {
      return $this->belongsTo('App\Payment','payment_id');
   }

   public function dentist()
   {
      return $this->belongsTo('App\User','dentist_id');
   }
   
   public function treatmentType()
   {
      return $this->belongsTo('App\TreatmentType','treatment_type_id');
   }

}
