<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemContact extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'item_contacts';

 /**
 * The attributes that are mass assignable.
 *
 * @var array
 */
  protected $fillable = [
    'title',
    'phone1',
    'phone2',
    'contact_type'
  ];

}
