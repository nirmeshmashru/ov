<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    //
    //

   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'appointments';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
      protected $fillable = [
         'clinic_id', 'dentist_id', 'patient_id','appointment_type_id','treatment_type_id','appointment_starttime','appointment_endtime','starttimestamp','endtimestamp','appointment_observation','startdate','status','dental_plan_id','created_by'
      ];

      public function patient()
      {
         return $this->belongsTo('App\Patient','patient_id');
      }
      public function dentist()
      {
         return $this->hasMany('App\Dentist','user_id','dentist_id');
      }
      public function payment()
      {
         return $this->belongsTo('App\Payment','patient_id');
      }
      public function documents()
      {
         return $this->belongsTo('App\PatientTreatmentImage','patient_id');
      }
      public function treatmentType()
      {
         return $this->belongsTo('App\TreatmentType','treatment_type_id');
      }
      public function treatment()
      {
         return $this->hasManyThrough('App\Payment', 'App\Treatment','payment_id','id');
         //return $this->belongsTo('App\Treatment','id','appointment_id');
      }
      public function speciality()
      {
         return $this->hasManyThrough('App\Speciality','App\TreatmentSpeciality','treatment_type_id','id');
      }


}
