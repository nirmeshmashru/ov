<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
   protected $table = 'address';
   protected $fillable = [
      'street_address','number','borough','zip','city','state','country','borough_id','city_id','state_id'
   ];
}
