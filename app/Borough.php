<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Borough extends Model
{
    //
   protected $table = 'boroughs';

   /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
   protected $fillable = [
     'title','city_id','status'
   ];
}
