<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holidays extends Model
{
    //
    //
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'appointment_holidays';

  /**
  * The attributes that are mass assignable.
  *
  * @var array
  */
   protected $fillable = [
     'starttimestamp',
     'endtimestamp',
     'start',
     'end',
     'user_id',
   ];
}
