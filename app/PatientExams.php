<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientExams extends Model
{
   //
   //
   protected $table = 'patient_exams';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'patient_id',
     'appointment_id',
     'dentist_id',
     'exam_description',
     'exam_model',
     'model_id',
     'exam_date'
   ];
}
