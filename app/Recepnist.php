<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recepnist extends Model
{
    //
    protected $table = 'recepnists';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'clinic_id',
     'profile_url',
     'first_name',
     'last_name',
     'DOB',
     'gender',
     'email',
     'address_id',
     'contact_id',
   ];

   public function user()
   {
       return $this->belongsTo('App\User','user_id');
   }
   public function clinic()
   {
       return $this->belongsTo('App\Clinic');
   }

   public function contact()
   {
       return $this->belongsTo('App\Contact','contact_id');
   }
   public function address()
   {
       return $this->belongsTo('App\Address','address_id');
   }
}
