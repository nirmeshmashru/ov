<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppointmentHolidays extends Model
{
    //
    //
   /**
    * The table associated with the model.
    *
    * @var string
    */
   protected $table = 'appointment_holidays';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
     protected $fillable = [
        'starttimestamp',
        'endtimestamp',
        'start_date',
        'end_date',
        'user_id'
     ];
}
