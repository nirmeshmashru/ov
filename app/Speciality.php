<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
   protected $table = 'specialities';

   /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
   protected $fillable = [
     'title','description','color_code'
   ];
}
