<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientDisease extends Model
{
    //
    protected $table = 'patient_disease';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'patient_id',
     'disease_id',
     'status'
   ];
}
