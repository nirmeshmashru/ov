<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientTreatmentImage extends Model
{
    //
    //
    protected $table = 'patient_treatment_images';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
     'patient_id',
     'appointment_id',
     'url'
   ];
}
