<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class PatientQuotation extends Model
{
   //
   protected $table = 'patient_quotation';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
      'patient_id',
      'appointment_id',
      'content'
   ];
}
