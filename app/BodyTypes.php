<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BodyTypes extends Model
{
    //
    //

   /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'body_types';

   /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
      protected $fillable = [
         'title'
      ];
}
