<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientSpeciality extends Model
{
  /**
   * The table associated with the model.
   *
   * @var string
   */
  protected $table = 'patient_speciality';

   /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
   protected $fillable = [
      'patient_id',
      'speciality_id'
   ];
}
