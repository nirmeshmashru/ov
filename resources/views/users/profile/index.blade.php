@extends('layouts.page')
@section('content')
<div class="main-content">
	<!-- start: MAIN CONTAINER -->
   <div class="container" style="margin-top: -25px">

      <!-- start: MAIN INFORMATION PANEL -->
      <div class="panel panel-white" style="margin-top: 50px">

		  <!-- start: PAGE HEADER TITLE -->
        <div class="panel-heading" style="padding: 5px 25px">
			<h1 style="font-weight: lighter">{{ $title }} <br><small style="font-weight: lighter;color: #d1c4e9">{{ $subtitle }}</small></h1>
			<hr style="border-top: 1px #dddddd solid">
		</div>
		  <!-- end: PAGE HEADER TITLE -->

		  <!-- start: BODY INFORMATION -->
         <div class="panel-body">
            {{ Form::model($data, ['route' => ['users.update', $data->id], 'method' => 'put','id' => 'updateDentist']) }}

               @if(count($errors))
                  <div class="alert alert-danger">
                     <strong>Whoops!</strong> There were some problems with your input.
                     <br>
                     <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
               @endif
               <input type="hidden" name="id" value="{{ $data->id }}">

               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
   					  <label for="fname">Nome</label>
                    {{ Form::text('first_name',$data->first_name,array('placeholder' => 'First Name','class' => 'form-control')) }}
   					</div>
   				</div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
   					  <label for="fname">Sobrenome</label>
   					  {{ Form::text('last_name',$data->last_name,array('placeholder' => 'Last Name','class' => 'form-control')) }}
   					</div>
   				</div>
               <div class="clearfix"></div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
   					  <label for="fname">E-mail</label>
   					 {{ Form::text('email',$data->email,array('placeholder' => 'This will be also your login','class' => 'form-control','disabled' => 'true')) }}
   					</div>
   				</div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
   					   <label for="fname">Sexo</label>
                     <div class="clearfix">
                        <label>{{ Form::radio('gender', '0') }} Masculino </label> &nbsp;
                        <label>{{ Form::radio('gender', '1') }} Feminino </label>
                     </div>
   					</div>
   				</div>

               <div class="clearfix"></div>

               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
   					  <label for="fname">Data de Nascimento</label>
   					   {{ Form::text('dob',$data->dob,array('placeholder' => 'Date of Birth','class' => 'form-control datepicker')) }}
   					</div>
   				</div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('cpf') ? 'has-error' : '' }}">
   					  <label for="fname">CPF</label>
   					  {{ Form::text('cpf',$data->cpf,array('placeholder' => 'CPF','class' => 'form-control')) }}
   					</div>
   				</div>
               <div class="clearfix"></div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('rg') ? 'has-error' : '' }}">
   					  <label for="fname">RG</label>
   					  {{ Form::text('rg',$data->rg,array('placeholder' => 'RG','class' => 'form-control')) }}
   					</div>
   				</div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('observation') ? 'has-error' : '' }}">
   					  <label for="fname">Observações</label>
   					   {{ Form::text('observation',$data->observation,array('placeholder' => 'Observation','class' => 'form-control')) }}
   					</div>
   				</div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('cro') ? 'has-error' : '' }}">
   					  <label for="fname">CRO</label>
   					   {{ Form::text('cro',$data->cro,array('placeholder' => 'CRO','class' => 'form-control')) }}
   					</div>
   				</div>
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('honors') ? 'has-error' : '' }}">
   					  <label for="fname">Honors</label>
   					   {{ Form::text('honors',$data->honors,array('placeholder' => 'Honors','class' => 'form-control')) }}
   					</div>
   				</div>

               <div class="col-md-12">
                  <h3 class="panel_inner_title" style="font-weight: lighter">Preferences e Outras Detalhes</h3>
				   <hr style="border-top: 1px #dddddd solid;margin-top: -10px">
               </div>

               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">

   					<div class="form-group {{ $errors->has('resident_in_clinic') ? 'has-error' : '' }}">

   					  <label style="font-size: 1.1em">Resident in Clinic &nbsp;&nbsp; {{ Form::checkbox('resident_in_clinic',1) }} </label>
   					</div>
                  <div class="form-group {{ $errors->has('use_whatapp') ? 'has-error' : '' }}">
   					  <label style="font-size: 1.1em">Use Whats App &nbsp;&nbsp; {{ Form::checkbox('use_whatsapp',1) }} </label>
   					</div>
                  <div class="form-group {{ $errors->has('accept_calls') ? 'has-error' : '' }}">
   					  <label style="font-size: 1.1em">Accept Calls &nbsp;&nbsp; {{ Form::checkbox('accept_calls',1) }} </label>
   					</div>
   				</div>

               <div class="clearfix"></div>
               <div class="col-md-12">
                  <hr>
               </div>
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
   					<div class="form-group">
                     <button class="btn btn-success btn-submit" data-loading-text="Saving..." type="submit">Save</button>
   					</div>
   				</div>
            </form>
         </div>
		  <!-- end: BODY INFORMATION -->

      </div>
	   <!-- end: MAIN INFORMATION PANEL -->
   </div>
	<!-- end: MAIN CONTAINER -->
</div>
@endsection
