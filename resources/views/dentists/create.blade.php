@extends('layouts.page')
@section('content')

    <!-- start: MAIN CONTENT -->
    <div class="main-content">

        <!-- start: CONTAINER -->
        <div class="container">

            <!-- start: MAIN INFORMATION PANEL -->
            <div class="panel panel-white" style="margin-top:8px;">

                <!-- start: FORM HEADER -->
                <div class="panel-heading header_t1" style="padding-bottom: 0;margin-bottom: 0">
                    <h2 class="table_title">{{ $title }} <br>
                        <small style="color: silver">{{ $subtitle }}</small>
                    </h2>
                    <hr class="custom_sep" style="padding-bottom: 0;margin-bottom: 0">
                </div>
                <!-- end: FORM HEADER -->

                <!-- start: PANEL BODY -->
                <div class="panel-body">

                    <!-- start: FORM -->
                    <form id="registerDentist" method="POST" action="{{ url('/dentists') }}" autocomplete="off"
                          enctype="multipart/form-data">

                        <!-- start: ERROR REPORT -->
                        @if(count($errors))
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> Houve algum erro com os dados.
                                <br>
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                       <!-- end: ERROR REPORT -->

                        <!-- start: FORM METHODS -->
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="_method" value="POST">
                        <!-- end: FORM METHODS -->

                        <?php
                        $user = Auth::user();
                        if(!$user->isdentistadmin()){ ?>
                       <!-- start: CLINIC SELECT -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('clinic_id') ? 'has-error' : '' }}">
                                <div class="clearfix"></div>
                                <input type="hidden" name="clinic_id" value="<?php echo $user->clinic_id;?>">

                                <label for="fname">Selecione a Clínica</label>
                                {!! Form::select('clinic_id', $clinics, 'Select A Clinic',['class' => 'form-control']) !!}
                                <div class="clearfix"></div>

                            </div>

                        </div>
                        <!-- end: CLINIC SELECT -->
                        <?php } ?>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2 class="custom_header" style="font-size: 140%">Detalhes do Login</h2>
                        </div>

                        <!-- start: SELECT ROLE -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="role">Tipo Usuário</label>
                                <select class="form-control" name="role">
                                    <option value="dentist">Dentista Limitado</option>
                                    <option value="dentistadmin">Dentista Administrador</option>
                                </select>
                            </div>
                        </div>
                        <!-- end: SELECT ROLE -->

                        <!-- start: EMAIL -->
                        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                                <label for="fname">Email</label>
                                <input class="form-control" id="email" name="email" type="text"
                                       value="{{ old('email') }}">
                            </div>
                        </div>
                        <!-- end: EMAIL -->

                        <div class="clearfix"></div>

                        <!-- start: PASSWORD -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label for="fname">Password</label>
                                <input class="form-control" id="password" name="password" type="password"
                                       value="{{ old('password') }}">
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                                <label for="fname">Confirm Password</label>
                                <input class="form-control" id="confirm_password" name="password_confirmation"
                                       type="password" value="{{ old('confirm_password') }}">
                            </div>
                        </div>
                        <!-- end: PASSWORD -->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2 class="custom_header" style="font-size: 140%">Dados Pessoais</h2>
                        </div>

                        <!-- start: FIRST NAME -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                                <label for="fname">Nome</label>
                                <input class="form-control" id="first_name" name="first_name" type="text"
                                       value="{{ old('first_name') }}">
                            </div>
                        </div>
                        <!-- end: FIRST NAME -->

                        <!-- start: SURNAME -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                                <label for="fname">Sobrenome</label>
                                <input class="form-control" id="last_name" name="last_name" type="text"
                                       value="{{ old('last_name') }}">
                            </div>
                        </div>
                        <!-- end: SURNAME -->

                        <!-- start: GENDER -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('gender') ? 'has-error' : '' }}">
                                <label for="fname">Sexo</label>
                                <select class="form-control" name="role">
                                    <option name="gender" value="0">Masculino</option>
                                    <option name="gender" value="1">Feminino</option>
                                </select>
                            </div>
                        </div>
                        <!-- end: GENDER -->

                        <!-- start: DOB -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }}">
                                <label for="fname">Data de Nascimento</label>
                                <input class="form-control datepicker" id="dob" name="dob" type="text"
                                       value="{{ old('dob') }}" style="margin-bottom:0;">
                            </div>
                        </div>
                        <!-- end: DOB -->

                        <!-- start: CPF -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('cpf') ? 'has-error' : '' }}">
                                <label for="fname">CPF</label>
                                <input class="form-control" id="cpf" name="cpf" type="text" value="{{ old('cpf') }}">
                            </div>
                        </div>
                        <!-- end: CPF -->

                        <!-- start: RG -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('rg') ? 'has-error' : '' }}">
                                <label for="fname">RG</label>
                                <input class="form-control" id="rg" name="rg" type="text" value="{{ old('rg') }}">
                            </div>
                        </div>
                        <!-- end: RG -->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2 class="custom_header" style="font-size: 140%">Dados Profissionais</h2>
                        </div>

                        <!-- start: CRO -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('cro') ? 'has-error' : '' }}">
                                <label for="fname">CRO</label>
                                <input class="form-control" id="cro" name="cro" type="text" value="{{ old('cro') }}">
                            </div>
                        </div>
                        <!-- end: CRO -->

                        <!-- start: SPECIALTIES -->
                        <div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('honors') ? 'has-error' : '' }}">
                                <label for="fname">Especialidades</label>
                                <input class="form-control" id="honors" name="honors" type="text"
                                       value="{{ old('honors') }}">
                            </div>
                        </div>
                        <!-- end: SPECIALTIES -->

                        <div class="clearfix"></div>

                        <!-- start: RENT -->
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                            <div class="form-group {{ $errors->has('resident_in_clinic') ? 'has-error' : '' }}">
                                <label><input type="checkbox" name="resident_in_clinic" value="1">&nbsp;&nbsp;Aluga Sala</label>
                            </div>
                        </div>
                        <!-- end: RENT -->

                        <!-- start: WHATSAPP -->
                        <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
                            <div class="form-group {{ $errors->has('use_whatapp') ? 'has-error' : '' }}">
                                <label><input type="checkbox" name="use_whatsapp" value="1">&nbsp;&nbsp;Uso
                                    Whatsapp</label>
                            </div>
                        </div>
                        <!-- end: WHATSAPP -->

                        <!-- start: PHONECALLS -->
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                            <div class="form-group {{ $errors->has('accept_calls') ? 'has-error' : '' }}">
                                <label><input type="checkbox" name="accept_calls" value="1">&nbsp;&nbsp;Aceito ligações
                                    no Celular</label>
                            </div>
                        </div>
                        <!-- end: PHONECALLS -->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2 class="custom_header" style="font-size: 140%">Contato</h2>
                        </div>

                        <!-- start: LANDLINE -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="fname1">Telefone</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <!-- end: LANDLINE -->

                        <!-- start: WHATSAPP -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="fname2">Whatsapp</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <!-- end: WHATSAPP -->

                        <!-- start: CEL 1 -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="fname3">Celular 1</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <!-- end: CEL 1 -->

                        <!-- start: CEL 2 -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="fname4">Celular 2</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <!-- end: CEL 2 -->

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2 class="custom_header" style="font-size: 140%">Endereço</h2>
                        </div>

                        <!-- start: ROAD -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                                <label for="address">Rua</label>
                                <input class="form-control" id="address" name="address" type="text" value="{{ old('address') }}">
                            </div>
                        </div>
                        <!-- end: ROAD -->

                        <!-- start: NUMBER -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="fname">Número</label>
                                <input class="form-control" type="text">
                            </div>
                        </div>
                        <!-- end: NUMBER -->

                        <!-- start: ZIP -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group">
                                <label for="fname">CEP</label>
                                <input class="form-control" id="zip" name="zip" type="text"
                                       value="{{ old('zip') }}">
                            </div>
                        </div>
                        <!-- end: ZIPS -->

                        <!-- start: BOROUGH -->
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('country') ? 'has-error' : '' }}">
                                <label for="fname">Bairro</label>
                                <input class="form-control" id="country" name="country" type="text"
                                       value="{{ old('country') }}">
                            </div>
                        </div>
                        <!-- end: BOROUGH -->

                        <!-- start: CITY -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                                <label for="fname">City</label>
                                <input class="form-control" id="city" name="city" type="text" value="{{ old('city') }}">
                            </div>
                        </div>
                        <!-- end: CITY -->

                        <!-- start: STATE -->
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                                <label for="fname">State</label>
                                <input class="form-control" id="state" name="state" type="text" value="{{ old('state') }}">
                            </div>
                        </div>
                        <!-- end: STATE -->

                        <!-- start: OBS -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group {{ $errors->has('observation') ? 'has-error' : '' }}">
                                <label for="fname">Observação</label>
                                <input class="form-control" id="observation" name="observation" type="text"
                                       value="{{ old('observation') }}">
                            </div>
                        </div>
                        <!-- end: OBS -->

                        <div class="clearfix"></div>

                        <!-- start: BUTTON INTERACTIONS -->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

                            <hr class="custom_sepg">

                            <div class="form-group">
                                <button class="btn btn-success btn-submit" data-loading-text="Salvando..." type="submit">
                                    Salvar Cadastro
                                </button>
                            </div>

                        </div>
                        <!-- end: BUTTON INTERACTIONS -->

                    </form>
                    <!-- end: FORM -->

                </div>
                <!-- end: PANEL BODY -->

            </div>
            <!-- end: MAIN INFORMATION PANEL -->

        </div>
        <!-- end: CONTAINER -->

    </div>
    <!-- end: MAIN CONTENT -->

@endsection
