@extends('layouts.page')
@section('content')
<div class="main-content">
   <div class="container">

       <!-- start: MAIN INFORMATION PANEL -->
      <div class="row" style="margin-top: 25px">
         {{ Form::open(array('route' => 'dentalplans.store', 'class' => 'form', 'id' => 'createDentalPlan')) }}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="errorHandler alert alert-danger no-display">
                  <i class="fa fa-remove-sign"></i> Existem errors no formulário. Por favor verifique em baixo.
               </div>
            </div>


            <!-- start: INFORMATION PANEL -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">


               <div id="personal_details" class="tab-pane fade active in">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel">

                           <!-- start: TABLE HEADER -->
                           <div class="panel-heading header_t1">
                              <h2 class="table_title">{{ $title }}</h2>
                              <hr class="custom_sep">
                           </div>
                           <!-- end: TABLE HEADER -->

                           <div class="panel-body">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Insurer Name </label>
                                          <input class="form-control" id="title" name="title"  type="text" placeholder="Insurer Name">
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Site</label>
                                          <input class="form-control" id="url" name="url" type="text" placeholder="Website URL">
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Phone 1</label>
                                          <input class="form-control" id="phone" name="phone" type="text" placeholder="Phone 1">
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Phone 2</label>
                                          <input class="form-control" id="phone2" name="alternate_phone" type="text" placeholder="Phone 2">
                                       </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Address</label>
                                          <input class="form-control" id="address" name="address" type="text" placeholder="Address">
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Number</label>
                                          <input class="form-control" id="number" name="number" type="text" placeholder="Number">
                                       </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Borough</label>
                                          {!! Form::select('borough_id',$borough,'',['class' => 'select2picker select_borough','placeholder' => 'Select a Borough']) !!}
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">CEP</label>
                                          <input class="form-control" id="cep" name="cep" type="text" placeholder="CEP">
                                       </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="form-field-select-3">
                                             State
                                          </label>
                                          {!! Form::select('state_id',$states,'',['class' => 'select2picker select_state','placeholder' => 'Select a State']) !!}
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                         <label for="patient_city">City</label>
                                         {!! Form::select('city_id',array(''),'',['class' => 'select2picker select_city','placeholder' => 'Select a City']) !!}
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr class="custom_sep">
                                       <div class="form-group">
                                           <a href="{{ url('/dentalplans')}}" class="btn btn-info">
                                               Voltar
                                           </a>
                                         <button type="submit" class="btn btn-success">
                                             Salvar
                                         </button>
                                       </div>
                                    </div>
                                  </div>
                               </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>
           <!-- end: INFORMATION PANEL -->

        </form>
      </div>
       <!-- end: MAIN INFORMATION PANEL -->
   </div>
</div>

@endsection
