@extends('layouts.page')
@section('content')
<div class="main-content">
   <div class="container">

      <!-- TOOLBAR -->
      <div class="toolbar row">
   		<div class="col-sm-6 hidden-xs">
   			<div class="page-header">
   				<h1>{{ $title }} <small>{{ $subtitle }}</small></h1>
   			</div>
   		</div>
   		<div class="col-sm-6 col-xs-12">
   			<div class="toolbar-tools pull-right">
   				<!-- start: TOP NAVIGATION MENU -->
   				<ul class="nav navbar-right">
   					<li>
   						<a href="{{ url('/dentalplans')}}" class="new-event MyToolbar" >
   							<i class="fa fa-medkit"></i> View All Plans
   						</a>
   					</li>
   				</ul>
   				<!-- end: TOP NAVIGATION MENU -->
   			</div>
   		</div>
          <hr class="custom_sep">
   	</div>
      <!-- TOOLBAR -->

      <div class="row">
			<div class="col-md-12">
				<ol class="breadcrumb">
					<li>
						<a href="#">
							Dashboard
						</a>
					</li>
					<li class="active">
						{{ $title }}
					</li>
				</ol>
			</div>
		</div>

      <div class="row">
         {{ Form::model($plan, ['route' => ['dentalplans.update', $plan->id], 'method' => 'PUT','id' => 'updateDentalPlan']) }}
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div class="errorHandler alert alert-danger no-display">
                  <i class="fa fa-remove-sign"></i> Existem errors no formulário. Por favor verifique em baixo.
               </div>
            </div>

            <!-- MAIN WINDOW -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
               <div id="personal_details" class="tab-pane fade active in">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel">
                           <div class="panel-body">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Insurer Name </label>
                                          <input class="form-control" id="title" name="title"  type="text" placeholder="Insurer Name" value="{{ $plan->title }}">
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Site</label>
                                          <input class="form-control" id="url" name="url" type="text" placeholder="Website URL" value="{{ $plan->url }}">
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Phone 1</label>
                                          <input class="form-control" id="phone" name="phone" type="text" placeholder="Phone 1" value="{{ $plan->phone }}">
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Phone 2</label>
                                          <input class="form-control" id="phone2" name="alternate_phone" type="text" placeholder="Phone 2" value="{{ $plan->alternate_phone }}">
                                       </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Address</label>
                                          <input class="form-control" id="address" name="address" type="text" placeholder="Address" value="{{ $plan->address }}">
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Number</label>
                                          <input class="form-control" id="number" name="number" type="text" placeholder="Number" value="{{ $plan->number }}">
                                       </div>
                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">Borough</label>
                                          {!! Form::select('borough_id',$borough,$plan->borough_id,['class' => 'select2picker select_borough','placeholder' => 'Select a Borough']) !!}
                                       </div>
                                    </div>
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="fname">CEP</label>
                                          <input class="form-control" id="cep" name="cep" type="text" placeholder="CEP" value="{{ $plan->cep }}">
                                       </div>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                          <label for="form-field-select-3">
                                             State
                                          </label>
                                          {!! Form::select('state_id',$states,$plan->state_id,['class' => 'select2picker select_state','placeholder' => 'Select a State']) !!}
                                       </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                       <div class="form-group">
                                         <label for="patient_city">City</label>
                                         {!! Form::select('city_id',$cities,$plan->city_id,['class' => 'select2picker select_city','placeholder' => 'Select a City']) !!}
                                       </div>
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <hr>
                                       <div class="form-group">
                                         <button type="submit" class="btn btn-success">
                                             Update Dental Plan
                                         </button>
                                       </div>
                                    </div>
                                  </div>
                               </div>
                           </div>
                        </div>
                     </div>

                  </div>
               </div>
            </div>

        </form>
      </div>

   </div>
</div>

@endsection
