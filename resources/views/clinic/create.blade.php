@extends('layouts.page')
@section('content')

    <!-- start: MAIN CONTENT -->
<div class="main-content">

    <!-- start: CONTAINER -->
   <div class="container">

	   <!-- start: MAIN INFORMATION PANEL -->
      <div class="panel panel-white" style="margin-top:8px;">

          <!-- start: PANEL HEADING -->
         <div class="panel-heading header_t1" style="margin-bottom: 0px">
            <h2 class="table_title">Clínica<br> <small style="color: #dddddd">Criar cadastro da sua clínica</small></h2>
             <hr class="custom_sep" style="padding: 0;margin: 0">
         </div>
          <!-- end: PANEL HEADING -->

          <!-- start: PANEL BODY -->
         <div class="panel-body" style="margin-top: 0px;padding-top: 0px">

             <!-- start: FORM -->
            <form method="POST" action="{{ url('/clinic') }}" autocomplete="off" enctype="multipart/form-data">

                <!-- start: ERRORS -->
               @if(count($errors))
                  <div class="alert alert-danger">
                     <strong>Whoops!</strong> There were some problems with your input.
                     <br/>
                     <ul>
                        @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                     </ul>
                  </div>
               @endif
               <!-- end: ERRORS -->

               <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3 class="custom_header">Clinic Details</h3>
                </div>

                <!-- start: CLINIC NAME -->
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
   					  <label for="fname">Clinic Name</label>
   					  <input class="form-control" id="clinic_name" name="name" type="text" placeholder="Clinic Name" value="{{ old('name') }}">
   					</div>
   				</div>
                <!-- end: CLINIC NAME -->

                <!-- start: CLINIC ADDRESS -->
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
   					  <label for="fname">Clinic Address</label>
   					  <input class="form-control" id="address" name="address"  type="text" placeholder="Clinic Address" value="{{ old('address') }}">
   					</div>
   				</div>
                <!-- end: CLINIC ADDRESS -->

               <div class="clearfix"></div>

                <!-- start: CLINIC ADDRESS -->
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
   					  <label for="fname">Clinic Email</label>
   					  <input class="form-control" id="clinic_email" name="email"  type="text" placeholder="Clinic Email" value="{{ old('email') }}">
   					</div>
   				</div>
                <!-- end: CLINIC ADDRESS -->

                <!-- start: CLINIC PHONE -->
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
   					<div class="form-group {{ $errors->has('telephone') ? 'has-error' : '' }}">
   					  <label for="fname">Clinic Phone</label>
   					  <input class="form-control" id="clinic_phone" name="telephone"  type="text" placeholder="Clinic Phone" value="{{ old('telephone') }}">
   					</div>
   				</div>
                <!-- end: CLINIC PHONE -->

               <div class="clearfix"></div>

                <!-- start: CLINIC LOGO -->
               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
   					<div class="form-group {{ $errors->has('telephone') ? 'has-error' : '' }}">
   					  <label for="fname">Clinic Logo</label>
   					  <input id="logo" name="logo" value="{{ old('logo') }}" type="file">
   					</div>
   				</div>
                <!-- end: CLINIC LOGO -->

               <div class="clearfix"></div>

               <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

                   <hr class="custom_sepg">

   					<div class="form-group">
                     <button class="btn btn-success btn-squared" type="submit">Create Clinic</button>
   					</div>

   				</div>

            </form>
             <!-- end: FORM -->

         </div>
          <!-- end: PANEL BODY -->

      </div>
	   <!-- end: MAIN INFORMATION PANEL -->

   </div>
    <!-- end: CONTAINER -->

</div>
    <!-- end: MAIN CONTENT -->

@endsection
