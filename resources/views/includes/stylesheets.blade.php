<!-- start: MAIN CSS -->
<link rel="stylesheet" href="{{ url('/') }}/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/animate.css/animate.min.css">
<!-- end: MAIN CSS -->
<!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
<link rel="stylesheet" href="{{ url('/') }}/plugins/owl-carousel/owl-carousel/owl.carousel.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/owl-carousel/owl-carousel/owl.theme.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/owl-carousel/owl-carousel/owl.transitions.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/summernote/dist/summernote.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/fullcalendar/fullcalendar.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/datepicker/css/datepicker.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/bootstrap-datetimepicker-master/build/css/bootstrap-datetimepicker.min.css">

<link rel="stylesheet" href="{{ url('/') }}/plugins/toastr/toastr.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/sweetalert/lib/sweet-alert.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/bootstrap-select/bootstrap-select.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/DataTables/media/css/DT_bootstrap.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/DataTables/media/css/buttons.dataTables.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
<!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
<link href="{{ url('/') }}/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css">
<link href="{{ url('/') }}/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/weather-icons/css/weather-icons.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/nvd3/nv.d3.min.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/select2/select2.css">
<link rel="stylesheet" href="{{ url('/') }}/plugins/fileinput/css/fileinput.min.css">
<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
<!-- start: CORE CSS -->
<link rel="stylesheet" href="{{ url('/') }}/css/styles.css">
<link rel="stylesheet" href="{{ url('/') }}/css/styles-responsive.css">
<link rel="stylesheet" href="{{ url('/') }}/css/plugins.css">
<link rel="stylesheet" href="{{ url('/') }}/css/themes/theme-style8.css" type="text/css" id="skin_color">
<link rel="stylesheet" href="{{ url('/') }}/css/print.css" type="text/css" media="print"/>
<link rel="stylesheet" href="{{ url('/') }}/css/custom.css">
