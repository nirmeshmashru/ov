<!-- Scripts -->
<script type="text/javascript" src="{{ url('/') }}/plugins/jQuery/jquery-2.1.1.min.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/iCheck/jquery.icheck.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/moment/min/moment.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootbox/bootbox.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery.appear/jquery.appear.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery-cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/velocity/jquery.velocity.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<script type="text/javascript" src="{{ url('/') }}/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery-mockjax/jquery.mockjax.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/toastr/toastr.js"></script>
<script src="{{ url('/') }}/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-datetimepicker-master/build/js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/truncate/jquery.truncate.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/summernote/dist/summernote.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/subview.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/main1.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/validations.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/ajax-actions.js"></script>
