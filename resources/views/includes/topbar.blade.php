
<div id="slidingbar-area">
	<div id="slidingbar">
		<div class="row">
			<!-- start: SLIDING BAR THIRD COLUMN -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<center>
				<h2>My Info</h2>
				<address class="margin-bottom-40">
					<?php
						$clinic_info = userDetails();
						 ?>
					<?php echo $clinic_info->name;?>
					<br>
					Email:
					<a href="mailto:{{$clinic_info->email}}">
						<?php echo $clinic_info->email; ?>
					</a>
				</address>
				</center>
			</div>
			<!-- end: SLIDING BAR THIRD COLUMN -->
		</div>
		<div class="row">
			<!-- start: SLIDING BAR TOGGLE BUTTON -->
			<div class="col-md-12 text-center">
				<a href="#" class="sb_toggle"><i class="fa fa-chevron-up"></i></a>
			</div>
			<!-- end: SLIDING BAR TOGGLE BUTTON -->
		</div>
	</div>
</div>
