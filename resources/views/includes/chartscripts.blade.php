<script type="text/javascript" src="{{ url('/') }}/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/Chart.js"></script>
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.js"></script-->
<script type="text/javascript" src="{{ url('/') }}/plugins/nvd3/lib/d3.v3.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/nvd3/nv.d3.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/nvd3/src/models/historicalBar.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/nvd3/src/models/historicalBarChart.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/nvd3/src/models/stackedArea.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/nvd3/src/models/stackedAreaChart.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/jquery.sparkline/jquery.sparkline.js"></script>
<script type="text/javascript" src="{{ url('/') }}/plugins/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script type="text/javascript" src="{{ url('/') }}/js/index.js"></script>
