<a class="closedbar inner hidden-sm hidden-xs" href="#"></a>
<?php $user = Auth::user();?>
<nav id="pageslide-left" class="pageslide inner">
	<div class="navbar-content">
		<!-- start: SIDEBAR -->
		<div class="main-navigation left-wrapper transition-left">
			<div class="user-profile border-top padding-horizontal-10 block">
				<div class="padding-vertical-20">
					<div class="">
						<div class="profile_block">
							<div class="profile_image">
								<img src="{{ url('/') }}/images/user/male.png" alt="" style="opacity: 0.85">
							</div>
							<div class="profile_information">
								<!-- start: DEPENDING HOUR SEND GOOD MORNING - GOOD AFTERNOON - GOOD NIGHT -->
								<h5 class="no-margin">
									<?php

                                    /* This sets the $time variable to the current hour in the 24 hour clock format */
                                    $time = date("H");
                                    /* If the time is less than 1200 hours, show good morning */
                                    if ($time < "12") {
                                        echo "Bom dia";
                                    } else
                                        /* If the time is grater than or equal to 1200 hours, but less than 1700 hours, so good afternoon */
                                        if ($time >= "12" && $time < "17") {
                                            echo "Boa tarde";
                                        } else
                                            /* Should the time be between or equal to 1700 and 1900 hours, show good evening */
                                            if ($time >= "17" && $time < "19") {
                                                echo "Boa Noite";
                                            } else
                                                /* Finally, show good night if the time is greater than or equal to 1900 hours */
                                                if ($time >= "19") {
                                                    echo "Boa Noite";
                                                }
                                    ?>
								</h5>
								<!-- end: DEPENDING HOUR SEND GOOD MORNING - GOOD AFTERNOON - GOOD NIGHT -->
								<h4>
									{{ Auth::user()->name }}
								</h4>
								<a class="btn user-options sb_toggle" data-toggle="tooltip" data-placement="bottom" title="Dados da Clinica">
									<i style="font-size:1.0em!important" class="fa fa-cog"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- start: MAIN NAVIGATION MENU -->
			<ul class="main-navigation-menu">
				<li class="<?php if(isset($activeClass)){if($activeClass == 'dashboard'){echo 'active open';}}?>" style="margin-top:1px;">
					<a href="{{ url('/home')}}"><i class="fa fa-tachometer"></i> <span class="title"> Dashboard </span><!--<span class="label label-default pull-right ">LABEL</span> --> </a>
				</li>

				<?php if($user->hasPermission('calendarcontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'appointments'){echo 'active open';}}?>">
					<a href="javascript:void(0)"><i class="fa fa-calendar"></i><span class="title"> Appointments </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<?php if($user->hasPermission('calendarcontroller.index')){ ?>
						<li class="">
							<a href="{{ url('/calendar') }}">
								<span class="title"> Book An Appointment</span>
							</a>
						</li>
						<!-- <li class="">
							<a href="{{ url('/holidays') }}">
								<span class="title"> Holiday Slots</span>
							</a>
						</li> -->
						<?php } ?>
						<li class="">
							<a href="{{ url('/agenda') }}">
								<span class="title"> Appointment Settings</span>
							</a>
						</li>
						<?php if($user->isAdmin() || $user->hasPermission('appointmenttypesrcontroller.index')){ ?>
						<li>
							<a href="{{ url('/calendar/appointmentTypes') }}">
								<span class="title"> Appointment Types</span>
							</a>
						</li>
						<?php } ?>
					</ul>
				</li>
				<?php } ?>
				<?php  if($user->isAdmin()){ ?>
					<li class="<?php if(isset($activeClass)){if($activeClass == 'clinic'){echo 'active open';}}?>">
						<a href="javascript:void(0)"><i class="fa fa-h-square"></i><span class="title"> Clinics </span><i class="icon-arrow"></i> </a>
						<ul class="sub-menu">
							<li class="">
								<a href="{{ url('/clinic') }}">
									<span class="title"> Clinics</span>
								</a>
							</li>
							<li>
								<a href="{{ url('/clinic/create') }}">
									<span class="title"> Add Clinic</span>
								</a>
							</li>
						</ul>
					</li>
				<?php } ?>
				<?php if($user->hasRole('dentistadmin')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'users_management'){echo 'active open';}}?>">
					<a href="{{ url('/users/manage') }}"><i class="fa fa-users"></i><span class="title"> Users Management </span></a>
				</li>
				<?php } ?>
				<?php if($user->isAdmin()){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'users'){echo 'active open';}}?>" style="display:none;">
					<a href="javascript:void(0)"><i class="fa fa-user"></i><span class="title"> Users </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li class="">
							<a href="{{ url('/users') }}">
								<span class="title"> Users</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/users/create') }}">
								<span class="title"> Add User</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('dentistscontroller.index')){ ?>
					<li class="<?php if(isset($activeClass)){if($activeClass == 'dentists'){echo 'active open';}}?>">
						<a href="javascript:void(0)"><i class="fa fa-user-md"></i><span class="title"> Dentists </span><i class="icon-arrow"></i> </a>
						<ul class="sub-menu">
							<li class="">
								<a href="{{ url('/dentists') }}">
									<span class="title"> Dentists</span>
								</a>
							</li>
							<li>
								<a href="{{ url('/dentists/create') }}">
									<span class="title"> Add Dentist</span>
								</a>
							</li>
						</ul>
					</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('patientscontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'patients'){echo 'active open';}}?>" >
					<a href="javascript:void(0)"><i class="fa fa-users"></i><span class="title"> Patients </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<?php if($user->hasRole('dentistadmin')){ ?>
							<li class="">
								<a href="{{ url('/patients/stats') }}">
									<span class="title"> Statistics</span>
								</a>
							</li>
						<?php } ?>
						<li class="">
							<a href="{{ url('/patients') }}">
								<span class="title"> Patients</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/patients/create') }}">
								<span class="title"> Add Patient</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<?php if($user->hasPermission('recepnistscontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'recepnists'){echo 'active open';}}?>">
					<a href="javascript:void(0)"><i class="fa fa-user"></i><span class="title"> Receptionists </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li class="">
							<a href="{{ url('/recepnists') }}">
								<span class="title"> Receptionists</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/recepnists/create') }}">
								<span class="title"> Add New</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('consultationcontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'consultation'){echo 'active open';}}?>">
					<a href="javascript:void(0)"><i class="fa fa-user"></i><span class="title"> Consultation </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li class="">
							<a href="{{ url('/consultation') }}">
								<span class="title"> Consultation</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/consultation/create') }}">
								<span class="title"> Add New</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('treatmenttypescontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'treatmenttypes'){echo 'active open';}}?>" >
					<a href="javascript:void(0)"><i class="fa fa-folder-open"></i> <span class="title"> Treatments </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="{{ url('/')}}/treatmenttypes">
								<span class="title">Treatments</span>
							</a>
						</li>
						<?php if($user->isAdmin() || $user->hasPermission('treatmenttypescontroller.create')){ ?>
						<li>
							<a href="{{ url('/')}}/treatmenttypes/create">
								<span class="title">Register</span>
							</a>
						</li>
						<?php } ?>
						<?php if($user->isAdmin() ){ ?>
						<li>
							<a href="{{ url('/')}}/specialities">
								<span class="title">Specialities</span>
							</a>
						</li>
						<?php } ?>
					</ul>
				</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('dentalplanscontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'dentalplans'){echo 'active open';}}?>" >
					<a href="javascript:void(0)"><i class="fa fa-folder-open"></i> <span class="title"> Dental Plans </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="{{ url('/')}}/dentalplans">
								<span class="title">Dental Plans</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/')}}/dentalplans/create">
								<span class="title">Add New</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('remindercontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'reminders'){echo 'active open';}}?>" >
					<a href="{{ url('/')}}/reminders"><i class="fa fa-bell"></i> <span class="title"> Reminders </span> <span class="badge badge-info reminderCount" >{{ Auth::user()->reminderCount }}</span></a>
					<!-- <ul class="sub-menu">
						<li>
							<a href="{{ url('/')}}/reminders">
								<span class="title">Reminders</span>
							</a>
						</li>
					</ul> -->
				</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('paymentcontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'payments'){echo 'active open';}}?>" >
					<a href="javascript:void(0)"><i class="fa fa-money"></i> <span class="title"> Payments </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="{{ url('/')}}/payments">
								<span class="title">Payments</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/')}}/payments/in">
								<span class="title">Payments In</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/')}}/payments/in">
								<span class="title">Payments Out</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>


				<?php  if($user->isAdmin()){ ?>
					<li class="<?php if(isset($activeClass)){if($activeClass == 'permissions'){echo 'active open';}}?>" >
						<a href="javascript:void(0)"><i class="fa fa-folder-open"></i> <span class="title"> Permissions </span><i class="icon-arrow"></i> </a>
						<ul class="sub-menu">
							<li>
								<a href="{{ url('/') }}/permissions">
									<span class="title"> Role Permissions</span>
								</a>
							</li>
							<li>
								<a href="{{ url('/') }}/permissions/create">
									<span class="title"> Add Permission</span>
								</a>
							</li>
							<li>
								<a href="{{ url('/') }}/permissions/assignPermissions">
									<span class="title"> Assign Permissions</span>
								</a>
							</li>
						</ul>
					</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('stockcontrolcontroller.index')){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'stockcontrol'){echo 'active open';}}?>" >
					<a href="javascript:void(0)"><i class="fa fa-cog"></i> <span class="title"> Stock Control </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="{{ url('/stockcontrol') }}">
								<span class="title">All Items</span>
							</a>
						</li>
						<li>
							<a href="{{ route('stockcontrol.create') }}">
								<span class="title">Add New Item</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/quoteitems') }}">
								<span class="title">Quote Items</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'contacts'){echo 'active open';}}?>" >
					<a href="{{ route('contacts.index') }}"><i class="fa fa-cog"></i> <span class="title"> Contacts </span></a>
				</li>
				<?php } ?>
				<?php if($user->isAdmin()){ ?>
				<li class="<?php if(isset($activeClass)){if($activeClass == 'settings'){echo 'active open';}}?>" >
					<a href="javascript:void(0)"><i class="fa fa-cog"></i> <span class="title"> Settings </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="{{ url('/diseases') }}">
								<span class="title">Diseases</span>
							</a>
						</li>
						<li>
							<a href="{{ url('/bodytypes') }}">
								<span class="title">Body Types</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<?php if($user->isAdmin() || $user->hasPermission('paymentcontroller.index')){ ?>
				<li style="display:none;" class="<?php if(isset($activeClass)){if($activeClass == 'finances'){echo 'active open';}}?>" >
					<a href="javascript:void(0)"><i class="fa fa-money"></i> <span class="title"> Finances </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="admin_finance_stats.php">
								<span class="title"> Stats</span>
							</a>
						</li>
						<li>
							<a href="admin_finance_in.php">
								<span class="title"> In</span>
							</a>
						</li>
						<li>
							<a href="admin_finance_out.php">
								<span class="title"> Out</span>
							</a>
						</li>
						<li>
							<a href="dentist_payment_search.php">
								<span class="title"> Pending</span>
							</a>
						</li>
						<li>
							<a href="orto_simulator.php">
								<span class="title"> Orto Calculator</span>
							</a>
						</li>
					</ul>
				</li>
				<?php } ?>
				<!-- <li>
					<a href="javascript:void(0)"><i class="fa fa-bell"></i> <span class="title"> Reminders </span><i class="icon-arrow"></i> </a>

				</li>
				<li>
					<a href="javascript:void(0)"><i class="fa fa-money"></i> <span class="title"> Supplies </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="#">
								<span class="title"> Order List</span>
							</a>
						</li>
					</ul>
				</li> -->
				<!-- <li>
					<a href="admin_statistics.php"><i class="fa fa-line-chart"></i> <span class="title"> Statistics </span><i class="icon-arrow"></i> </a>

				</li>
				<li>
					<a href="javascript:void(0)"><i class="fa fa-building"></i> <span class="title"> Consultations </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="dentist_consultation_search.php">
								<span class="title"> Search</span>
							</a>
						</li>
						<li>
							<a href="dentist_ask_a_pro_edit.php">
								<span class="title"> Request</span>
							</a>
						</li>
					</ul>
				</li> -->
				<!-- <li>
					<a href="javascript:void(0)"><i class="fa fa-shopping-cart"></i> <span class="title"> Labs </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="admin_labs_search.php">
								<span class="title"> Search</span>
							</a>
						</li>
						<li>
							<a href="">
								<span class="title"> Orders </span>
							</a>
						</li>
					</ul>
				</li> -->
			</ul>
			<!-- end: MAIN NAVIGATION MENU -->
		</div>
		<!-- end: SIDEBAR -->
	</div>
	<div class="slide-tools">
		<div class="col-xs-6 text-left no-padding">
			<a class="btn btn-sm status" href="#">
				Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
			</a>
		</div>
		<div class="col-xs-6 text-right no-padding">
			<a class="btn btn-sm log-out text-right"  href="{{ url('/logout') }}" onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
				<i class="fa fa-power-off"></i> Log Out
			</a>
		</div>
	</div>
</nav>
