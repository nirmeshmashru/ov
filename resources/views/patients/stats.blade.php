@extends('layouts.dashboard')
@section('content')

<div class="main-content">

   <div class="container">

	   <!-- start: MAIN INFO PANEL -->
      <div class="row" style="margin-top: 10px">

         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 nopadding">
            <div class="panel-default panel-white core-box ">
               <div class="panel-body no-padding">
                  <div class="padding-20 partition-green core-icon text-center">
                     <h2>{{ $patients['count'] }}</h2>
                  </div>
                  <div class="padding-20 core-content">
                     <h4 class=" block no-margin">Patients</h4>
                     <span class="subtitle">Registered</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 nopadding">
            <div class="panel-default panel-white core-box ">
               <div class="panel-body no-padding">
                  <div class="padding-20 partition-green core-icon text-center">
                     <h2>0</h2>
                  </div>
                  <div class="padding-20 core-content">
                     <h4 class=" block no-margin">Patients</h4>
                     <span class="subtitle">Not active</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 nopadding">
            <div class="panel-default panel-white core-box ">
               <div class="panel-body no-padding">
                  <div class="padding-20 partition-green core-icon text-center">
                     <h2>0</h2>
                  </div>
                  <div class="padding-20 core-content">
                     <h4 class=" block no-margin">Average</h4>
                     <span class="subtitle">Age</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 nopadding">
            <div class="panel-default panel-white core-box ">
               <div class="panel-body no-padding">
                  <div class="padding-20 partition-green core-icon text-center">
                     <h2>0</h2>
                  </div>
                  <div class="padding-20 core-content">
                     <h4 class=" block no-margin">Patients</h4>
                     <span class="subtitle">Monthly Avg.</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 nopadding">
            <div class="panel-default panel-white core-box ">
               <div class="panel-body no-padding">
                  <div class="padding-20 partition-green core-icon text-center">
                     <h2>{{ $patients['insuredPlan'] }}</h2>
                  </div>
                  <div class="padding-20 core-content">
                     <h4 class=" block no-margin">Patients</h4>
                     <span class="subtitle">Insured</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 nopadding">
            <div class="panel-default panel-white core-box ">
               <div class="panel-body no-padding">
                  <div class="padding-20 partition-green core-icon text-center">
                     <h2>{{ $patients['privatePlan'] }}</h2>
                  </div>
                  <div class="padding-20 core-content">
                     <h4 class=" block no-margin">Patients</h4>
                     <span class="subtitle">Private</span>
                  </div>
               </div>
            </div>
         </div>


         <div class="clearfix"></div>

         <!-- start : PATIENTS BOOKED PER MONTH -->
         <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 nopadding">
            <div class="panel panel-white equalSecondRow">
               <div class="panel-heading border-light">
                  <h5 style="margin-bottom:0;">New Patients</h5>
               </div>
               <div class="panel-body">
                  <div class="convas-container">
                     <canvas  id="barChart"></canvas>
                  </div>
               </div>
            </div>
         </div>
         <!-- end : PATIENTS BOOKED PER MONTH -->
         <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 nopadding">
            <div class="panel panel-white equalSecondRow">
               <div class="panel-heading border-light">
                  <h5 style="margin-bottom:0;">Patients Attended</h5>
               </div>
               <div class="panel-body" style="width:100%">
                  <div class="convas-container">
                     <canvas  id="canvas1"></canvas>
                  </div>
               </div>
            </div>
         </div>


      </div>
	   <!-- end: MAIN INFO PANEL -->

   </div>

</div>

@endsection
