@extends('layouts.page')
@section('content')
<div class="main-content">
   <div class="container">

      <!-- start: MAIN PANEL -->
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-top: 15px">
         <!-- start: 1st ROW -->
         <div class="row">

            <!-- start: TAB OPTIONS -->
            <ul class="nav nav-tabs nav-justified nav-profile">
               <li class="active">
                  <a data-toggle="tab" href="#personal_details">
                     <strong>Details</strong>
                  </a>
               </li>
               <li >
                  <a data-toggle="tab" href="#health">
                     <strong>Health</strong>
                  </a>
               </li>
               <!-- <li  id="dentalPlanTitle">
                  <a data-toggle="tab" href="#dentalPlan">
                     <strong>Dental Plans</strong>
                  </a>
               </li> -->
               <!--<li id="ortoTitle">
                  <a data-toggle="tab" href="#orto">
                     <strong>Orto</strong>
                  </a>
               </li>
               <li id="prosTitle">
                  <a data-toggle="tab" href="#prosthesis">
                     <strong>Prosthesis</strong>
                  </a>
               </li>-->
               <li>
                  <a data-toggle="tab" href="#exam">
                     <strong>Exams</strong>
                  </a>
               </li>
            </ul>
            <!-- end: TAB OPTIONS -->

            {{ Form::open(array('route' => 'patients.store', 'class' => 'form', 'id' => 'addPatient', 'enctype' => 'multipart/form-data')) }}

              <!-- start: TAB CONTENT -->
               <div class="tab-content">

                  <!-- start: PERSONAL DETIALS -->
                  <div id="personal_details" class="tab-pane fade active in">
                     <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                           <div class="row">
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <center>
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                       <div class="fileupload-new thumbnail" style="width:100%">
                                       </div>
                                       <div style="line-height: 10px; width:100%" class="fileupload-preview fileupload-exists thumbnail">
                                       </div>
                                       <div>
                                          <span  class="btn btn-primary btn-file">
                                             <span class="fileupload-new">
                                                <i class="fa fa-picture-o"></i>
                                                Select image
                                             </span>
                                             <span class="fileupload-exists">
                                                <i class="fa fa-picture-o"></i>
                                                Change
                                             </span>
                                                <input name="patient_profile_image" id="patient_profile_image"  type="file" accept="image/x-png, image/gif, image/jpeg" >
                                          </span>
                                          <a href="#" class="btn fileupload-exists btn-light-grey" data-dismiss="fileupload">
                                             <i class="fa fa-times"></i> Remove
                                          </a>
                                       </div>
                                    </div>
                                 </center>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group">
                                    <label for="doc">Professional</label>
                                    {!! Form::select('professional_id', $professionals,'',['class' => 'form-control selectpicker','placeholder' => 'Select Professional']) !!}
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group">
                                   <label for="hasDentalPlan">Has Dental Plan</label>
                                   {!! Form::select('has_dental_plan', array('0' => 'No','1' => 'Yes'),'',['class' => 'form-control selectpicker']) !!}
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <input type="hidden" name="hasProsSpec" id="hasProsSpec" value="0"/>
                                 <input type="hidden" name="hasOrtoSpec" id="hasOrtoSpec" value="0"/>
                                 <div class="form-group">
                                    <label for="pSpec">Speciality</label>
                                    {!! Form::select('speciality[]',$treatments,'',['class' => 'form-control selectpicker','multiple' => 'true']) !!}
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="form-group">
                                    <label for="indication">Indication</label>
                                    {!! Form::select('indication', array('0' => 'Internet','1' => 'TV','2' => 'Newspaper'),'',['class' => 'form-control selectpicker','placeholder' => 'Select an Indication']) !!}
                                 </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                 <div class="checkbox">
                                    <label>
                                       {{ Form::checkbox('sms_confirmation','',null,array('class' => 'grey')) }}
                                       Confirmação SMS
                                    </label>
                                 </div>
                                 <div class="checkbox">
                                    <label>
                                       {{ Form::checkbox('allow_profile_use','',null,array('class' => 'grey')) }}
                                       Allow Profile Use
                                    </label>
                                 </div>
                                 <div class="checkbox">
                                    <label>
                                       {{ Form::checkbox('vip','',null,array('class' => 'grey')) }}
                                       VIP
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                           <ul class="nav nav-tabs nav-justified nav-profile nav-nested">
                              <li class="active">
                                 <a data-toggle="tab" href="#pDetails">
                                    <strong>Details</strong>
                                 </a>
                              </li>
                              <li >
                                 <a data-toggle="tab" href="#pContact">
                                    <strong>Contact</strong>
                                 </a>
                              </li>
                              <li>
                                 <a data-toggle="tab" href="#pAddress">
                                    <strong>Address</strong>
                                 </a>
                              </li>
                           </ul>
                           <div class="tab-content">
                              <div id="pDetails" class="tab-pane fade active in">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="fname">First Name</label>
                                            {{ Form::text('first_name','',array('placeholder' => 'First Name','class' => 'form-control')) }}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="sname">Surname</label>
                                            {{ Form::text('last_name','',array('placeholder' => 'Last Name','class' => 'form-control')) }}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="gender">Gender</label>
                                            {!! Form::select('gender', array('0' => 'Male','1' => 'Female','2' => 'Other'),'',['class' => 'form-control','placeholder' => 'Select a Gender']) !!}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                             <label for="dob">Date Of Birth</label>
                                             <div class="input-group">
                                                {{ Form::text('DOB','',array('placeholder' => 'DOB','class' => 'form-control    date-picker')) }}
                                                <span class="input-group-addon"> <i class="fa fa-calendar" ></i> </span>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="cpfv">CPF</label>
                                            {{ Form::text('CPF','',array('placeholder' => '###.###.###-##','class' => 'form-control', 'id' => 'cpfv')) }}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="rg">RG</label>
                                            {{ Form::text('RG','',array('placeholder' => '###.###.###-##','class' => 'form-control', 'id' => 'rg')) }}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="marital_status">Marital Status</label>
                                             {!! Form::select('maritial_status', array('0' => 'Unmarried','1' => 'Married','2' => 'Devorced','3' => 'Widow'),'',['class' => 'form-control','placeholder' => 'Select Maritial Status']) !!}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="nationality">Nationality</label>
                                            {{ Form::text('nationality','',array('placeholder' => 'Nationality','class' => 'form-control', 'id' => 'nationality')) }}
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div id="pContact" class="tab-pane fade">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <label for="phone_landline">
                                             Phone Landline
                                             <small class="text-warning">(999) 999-9999</small>
                                          </label>
                                          <div class="form-group">
                                             <div class="input-group">
                                                <span class="input-group-addon"> <i class="fa fa-phone"></i> </span>
                                                {{ Form::text('phone_landline','',array('placeholder' => 'Landline Number','class' => 'form-control input-mask-phone')) }}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <label for="phone_celular1">
                                             Phone Celular 1
                                             <small class="text-warning">
                                             (999) 999-9999
                                             </small>
                                          </label>
                                          <div class="form-group">
                                             <div class="input-group">
                                                <span class="input-group-addon"> <i class="fa fa-phone"></i> </span>
                                                {{ Form::text('celular_1','',array('placeholder' => 'Phone Cellular 1','class' => 'form-control input-mask-phone')) }}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <label for="phone_celular2">
                                             Phone Celular 2
                                             <small class="text-warning">
                                             (999) 999-9999
                                             </small>
                                          </label>
                                          <div class="form-group">
                                             <div class="input-group">
                                                <span class="input-group-addon"> <i class="fa fa-phone"></i> </span>
                                                {{ Form::text('celular_2','',array('placeholder' => 'Phone Cellular 2','class' => 'form-control input-mask-phone')) }}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group" style="padding-left:0px;padding-right:0px;margin-left:0px;margin-right:0px;">
                                             <label for="patient_whatsapp">
                                                Phone Whatsapp
                                             </label>
                                             <div class="input-group">
                                                <span class="input-group-addon"> <i class="fa fa-whatsapp"></i> </span>
                                                {{Form::text('whatsapp_number','',array('placeholder' => 'Whatsapp Number','class' => 'form-control input-mask-phone')) }}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                             <label for="patient_skype">
                                                Skype
                                             </label>
                                             <div class="input-group">
                                                <span class="input-group-addon"> <i class="fa fa-skype"></i> </span>
                                                {{Form::text('skype','',array('placeholder' => 'Skype','class' => 'form-control')) }}
                                             </div>
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                          <label for="email">Email</label>
                                             <div class="input-group">
                                             <span class="input-group-addon"> <i class="fa fa-envelope"></i> </span>
                                                {{ Form::text('email','',array('placeholder' => 'Email','class' => 'form-control input-mask-email')) }}
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div id="pAddress" class="tab-pane fade">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                    <div class="row">
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                          <div class="form-group">
                                            <label for="patient_road">Road/Avenue</label>
                                            {{Form::text('street_address','',array('placeholder' => 'Street Address','class' => 'form-control')) }}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                             <label for="form-field-select-3">
                                                Borough
                                             </label>
                                             {!! Form::select('borough_id',$borough,'',['class' => 'select2picker select_borough','placeholder' => 'Select a Borough']) !!}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                             <label for="form-field-select-3">
                                                State
                                             </label>
                                             {!! Form::select('state_id',$states,'',['class' => 'select2picker select_state','placeholder' => 'Select a State']) !!}
                                          </div>
                                       </div>
                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="patient_city">City</label>
                                            {!! Form::select('city_id',array(''),'',['class' => 'select2picker select_city','placeholder' => 'Select a City']) !!}
                                          </div>
                                       </div>


                                       <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="patient_number">Number</label>
                                            {{Form::text('number','',array('placeholder' => 'Number','class' => 'form-control')) }}
                                          </div>
                                       </div> -->

                                       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="patient_zip">Zip Code</label>
                                            {{Form::text('zip','',array('placeholder' => 'Zip','class' => 'form-control')) }}
                                          </div>
                                       </div>
                                       <!-- <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                          <div class="form-group">
                                            <label for="patient_obs">Observations</label>
                                            <textarea  col="20" rows="5" class="form-control" id="patient_obs" name="patient_obs" style="resize:none"></textarea>
                                          </div>
                                       </div> -->
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- end: PERSONAL DETIALS -->

                  <!-- start: HEALTH -->
                  <div id="health" class="tab-pane fade">
                     <div class="row" style="background:#fff;">
                              <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 nopadding">
                                 <div class="panel panel-white accepted_plan equalDivs">
                                    <div class="panel-body">
                                       <div class="row">
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                <label for="take_drug">Takes drugs or medicins?</label>
                                                <!--input type="text" class="form-control" id="take_drug" name="take_drug"-->
                                                {!! Form::select('take_drugs', array('0' => 'No','1' => 'Yes'),'',['class' => 'form-control selectpicker']) !!}
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                <label for="have_birth_defect">Birth Defects</label>
                                                {!! Form::select('has_birth_defect', array('0' => 'No','1' => 'Yes'),'',['class' => 'form-control selectpicker']) !!}
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                <label for="bone_dev_stage">Bone development stage?</label>
                                                {!! Form::select('bone_dev_stage', array('0' => 'No','1' => 'Yes'),'',['class' => 'form-control selectpicker']) !!}
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                <label for="take_prg_pills">Take pregnancy pills?</label>
                                                {!! Form::select('take_preg_pills', array('0' => 'No','1' => 'Yes'),'',['class' => 'form-control selectpicker']) !!}
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                <label for="has_prev_surgeries">Previouse surgeries?</label>
                                                {!! Form::select('has_prev_surgeries', array('0' => 'No','1' => 'Yes'),'',['class' => 'form-control selectpicker']) !!}
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                               <label for="current_health">Current health?</label>
                                               {!! Form::select('current_health', array('0' => 'Bad','1' => 'Ok','2' => 'Good'),'',['class' => 'form-control selectpicker']) !!}
                                            </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                <label for="wheel_chair">Wheelchair?</label>
                                                {!! Form::select('wheel_chair', array('0' => 'No','1' => 'Yes'),'',['class' => 'form-control selectpicker']) !!}
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <div class="form-group">
                                                <label for="body_type">Body Type?</label>
                                                {!! Form::select('body_type_id',$bodyTypes,'',['class' => 'form-control selectpicker']) !!}
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <label for="patient_height">Height?</label>
                                             <div class="form-group">
                                                <div class="input-group">
                                                    {{ Form::text('height','',array('placeholder' => 'Height','class' => 'form-control')) }}
                                                   <span class="input-group-addon">cm</span>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                             <label for="patient_weight">Weight?</label>
                                             <div class="form-group">
                                                <div class="input-group">
                                                    {{ Form::text('weight','',array('placeholder' => 'Weight','class' => 'form-control')) }}
                                                   <span class="input-group-addon">Kg</span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 nopadding">
                                 <input type="hidden" value="0" name="hasDisease" id="hasDisease" />
                                       <style>
                                          #diseaseList th, #diseaseList td {
                                             border-top: none !important;

                                             //border:1px solid red;
                                          }
                                          #diseaseList tbody > tr > td{
                                             vertical-align: top!important;
                                          }
                                          #diseaseList{
                                             width:100%;
                                          }
                                          input[name='diseases[]']{
                                             margin-right:10px;
                                          }
                                       </style>
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <?php foreach($disease as $data){ ?>
                                       <div class="col-md-6">
                                          <div class="form-group">
                                             <label>{{ Form::checkbox('disease','0','',array('class' => 'grey','data-id' => $data->id)) }} {{$data->title}}</label>
                                             <div class="clearfix"></div>
                                          </div>
                                       </div>
                                    <?php } ?>
                                 </div>
                              </div>
                           </div>
                  </div>
                  <!-- end: HEALTH -->

                  <!-- start: EXAMS -->
                  <div id="exam" class="tab-pane fade">
                     <div class="row" style="background:#fff;">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                           <div class="panel panel-white">
                              <div class="panel-body">
                                 <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 nopadding">
                                    <input type="file" id="input-id"  name="upload_exams[]"  class="file-loading" multiple accept="image/*, application/pdf"/>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- end: EXAMS -->

                  <div class="col-md-12">
                     <div class="row">
                        <br>
                        <div class="col-md-offset-10 col-md-2 col-sm-12 col-xs-12">
                           <div class="form-group">
                              <button  type="submit" name="save"  class="btn btn-block btn-success pull-right">
                                 Save
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>

               </div>
              <!-- start: TAB CONTENT -->

            </form>

         </div>
         <!-- end: 1st ROW -->
      </div>
      <!-- end: MAIN PANEL -->

   </div>
</div><br>
@endsection
