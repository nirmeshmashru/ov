@extends('layouts.page')
@section('content')

   <div class="main-content">
      <div class="container">

         <div id="msform">
            <!-- progressbar -->
            <ul id="progressbar">
               <li class="active">Basics</li>
               <li>Agenda</li>
               <li>Personal</li>
               <li>Contact Details</li>
               <li>Staff</li>
            </ul>

            <!-- fieldsets -->
            <fieldset>
               {{ Form::model($user, ['route' => ['dentists.update', $user->id], 'method' => 'PUT','id' => 'form1', 'enctype' => 'multipart/form-data']) }}
                  <h2 class="fs-title">Welcome to Odontovision</h2>
                  <h3 class="fs-subtitle">We will get you setup and working in no time.</h3>
                  <div class="form-group">
                     <input type="text" name="first_name" placeholder="First Name" value="{{ Auth::user()->first_name }}">
                  </div>
                  <div class="form-group">
                     <input type="text" name="last_name" placeholder="Surname" value="{{ Auth::user()->last_name }}" >
                  </div>
                  <div class="form-group">
                     <input type="text" placeholder="Email" value="{{ Auth::user()->email }}" disabled="">
                  </div>
                  <div class="form-group">
                     {!! Form::select('gender', array('0' => 'Male','1' => 'Female'),'array($patient->gender)',['class' => '','placeholder' => 'Select Gender']) !!}
                  </div>
                  <div class="form-group">
                     <input type="text" name="dob" placeholder="DOB" class="datepicker">
                  </div>
                  <input type="button" name="next" class="next action-button" value="Next" data-id="form1" />
               </form>
            </fieldset>
            <fieldset>

               {{ Form::open(array('route' => 'agenda.store','id' => 'form2')) }}
                  <input type="hidden" name="lunch_start" value="13:00">
                  <input type="hidden" name="lunch_end" value="13:30">
                  <input type="hidden" name="interval" value="13:30">
                  <h2 class="fs-title">Set your Agenda</h2>
                  <h3 class="fs-subtitle">Let Us know your schedule so we can create your agenda</h3>
                  <div class="row">
                     <div class="col-md-2">
                        <label>Monday</label>
                     </div>
                     <div class="col-md-2">
                        <label><input name="days[]" type="checkbox" checked></label>
                     </div>
                     <div class="col-md-4">
                        <select name="start[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 8 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                     <div class="col-md-4">
                        <select name="end[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 17 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                        <label>Tuesday</label>
                     </div>
                     <div class="col-md-2">
                        <label><input name="days[]" type="checkbox" checked></label>
                     </div>
                     <div class="col-md-4">
                        <select name="start[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 8 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                     <div class="col-md-4">
                        <select name="end[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 17 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                        <label>Wednesday</label>
                     </div>
                     <div class="col-md-2">
                        <label><input name="days[]" type="checkbox" checked></label>
                     </div>
                     <div class="col-md-4">
                        <select name="start[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 8 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                     <div class="col-md-4">
                        <select name="end[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 17 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                        <label>Thursday</label>
                     </div>
                     <div class="col-md-2">
                        <label><input name="days[]" type="checkbox" checked></label>
                     </div>
                     <div class="col-md-4">
                        <select name="start[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 8 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                     <div class="col-md-4">
                        <select name="end[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 17 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                        <label>Friday</label>
                     </div>
                     <div class="col-md-2">
                        <label><input name="days[]" type="checkbox" checked></label>
                     </div>
                     <div class="col-md-4">
                        <select name="start[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 8 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                     <div class="col-md-4">
                        <select name="end[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 17 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                        <label>Saturday</label>
                     </div>
                     <div class="col-md-2">
                        <label><input name="days[]" type="checkbox"></label>
                     </div>
                     <div class="col-md-4">
                        <select name="start[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 8 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                     <div class="col-md-4" >
                        <select name="end[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 17 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-2">
                        <label>Sunday</label>
                     </div>
                     <div class="col-md-2">
                        <label><input name="days[]" type="checkbox"></label>
                     </div>
                     <div class="col-md-4">
                        <select name="start[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 8 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                     <div class="col-md-4">
                        <select name="end[]">
                           @for($i = 0; $i < 24; $i++)
                             <option value="{{$i}}:00" {{ $i == 17 ? 'selected' : ''}}>{{ $i % 12 ? $i % 12 : 12 }}:00 {{ $i >= 12 ? 'pm' : 'am' }}</option>
                           @endfor
                        </select>
                     </div>
                  </div>
                  <input type="button" name="previous" class="previous action-button" value="Previous" />
                  <input type="button" name="next" data-id="form2" class="next action-button" value="Next" />
               </form>
            </fieldset>
            <fieldset>
               <form id="form3">
                  <h2 class="fs-title">Personal Details</h2>
                  <h3 class="fs-subtitle">Let Us Know about your professional details</h3>
                  <div class="form-group">
                     <input type="text" name="cro" placeholder="CRO" >
                  </div>
                  <div class="form-group">
                     <span class='label'>Select Specialities(hold shift key to select more than one)</span>
                     {!! Form::select('speciality',$specialities,'array($patient->gender)',['class' => '','multiple' => 'true']) !!}
                  </div>
                  <div class="form-group">
                     <input type="text" name="last_name" placeholder="CPF" >
                  </div>
                  <div class="form-group">
                     <input type="text" name="last_name" placeholder="RG" >
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-8">
                           <label>Do you want to work with percentages ?</label>
                        </div>
                        <div class="col-md-4">
                           <label><input type="checkbox" name="work_with_percentages"></label>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <div class="row">
                        <div class="col-md-8">
                           <label>Allow others to book appointments for you ?</label>
                        </div>
                        <div class="col-md-4">
                           <label><input type="checkbox" name="book_appointments_by_others"></label>
                        </div>
                     </div>
                  </div>
                  <input type="button" name="previous" class="previous action-button" value="Previous" />
                  <input type="button" name="next" class="next action-button" value="Next" />
               </form>
            </fieldset>
            <fieldset>
               <form id="form4">
                  <h2 class="fs-title">Contact Details</h2>
                  <h3 class="fs-subtitle">Let Us know about your contact details use.</h3>
                  <div class="form-group">
                     <input type="text" name="phone_landline" placeholder="Landline Number" >
                  </div>
                  <div class="form-group">
                     <input type="text" name="celular_1" placeholder="Celular 1" >
                  </div>
                  <div class="form-group">
                     <input type="text" name="celular_2" placeholder="Celular 2" >
                  </div>
                  <div class="form-group">
                     <input type="text" name="whats_app" placeholder="Whatsapp" >
                  </div>
                  <input type="button" name="previous" class="previous action-button" value="Previous" />
                  <input type="button" name="next" class="next action-button" value="Next" />
               </form>
            </fieldset>
            <fieldset>
               <form id="form5">
                  <h2 class="fs-title">Staff</h2>
                  <h3 class="fs-subtitle">Send user logins to other staff you wish to use the system.</h3>
                  <div class="row">
                     <div class="col-md-3">
                        <input type="text" name="first_name" placeholder="First Name">
                     </div>
                     <div class="col-md-3">
                        <input type="text" name="email" placeholder="Email">
                     </div>
                     <div class="col-md-3">
                        {!! Form::select('role_id', array('0' => 'Male','1' => 'Female'),'',['class' => '','placeholder' => 'Select Role']) !!}
                     </div>
                     <div class="col-md-3">
                        <input type="button" name="add" class="action-button" value="Add">
                     </div>

                  </div>
                  <input type="button" name="previous" class="previous action-button" value="Previous" />
                  <input type="submit" name="submit" class="submit action-button" value="Finish" />
               </form>
            </fieldset>
         </div>

<style>
   #pageslide-left{display: none;}
   .main-container.inner{margin-left:0;}
   /*form styles*/
   #msform {
   	width: 600px;
   	margin: 50px auto;
   	text-align: center;
   	position: relative;
   }
   #msform fieldset {
   	background: white;
   	border: 0 none;
   	border-radius: 3px;
   	box-shadow: 0 0 15px 1px rgba(0, 0, 0, 0.4);
   	padding: 20px 30px;
   	box-sizing: border-box;
   	width: 96%;
   	margin: 0 2%;

   	/*stacking fieldsets above each other*/
   	position: relative;
   }
   /*Hide all except first fieldset*/
   #msform fieldset:not(:first-of-type) {
   	display: none;
   }
   /*inputs*/
   #msform input, #msform textarea, #msform select{
   	padding: 15px;
   	border: 1px solid #ccc;
   	border-radius: 3px;
   	margin-bottom: 10px;
   	width: 100%;
   	box-sizing: border-box;
   	font-family: montserrat;
   	color: #2C3E50;
   	font-size: 13px;
   }
   #msform label{
   	font-family: montserrat;
   	color: #2C3E50;
      line-height: 40px;
   }
   #msform .form-group{
      margin:0;
   }
   #msform .form-group .help-block{
      color:#f24747;
      margin:0;
      margin-top:-7px;
      font-size:12px;
      text-align: left;
   }
   /*buttons*/
   #msform .action-button {
   	width: 100px;
   	background: #27AE60;
   	font-weight: bold;
   	color: white;
   	border: 0 none;
   	border-radius: 1px;
   	cursor: pointer;
   	padding: 10px 5px;
   	margin: 10px 5px;
   }
   #msform .action-button:hover, #msform .action-button:focus {
   	box-shadow: 0 0 0 2px white, 0 0 0 3px #27AE60;
   }

   #msform .label{
      font-family: montserrat;
   	color: #2C3E50;
   }
/*headings*/
.fs-title {
	font-size: 15px;
	text-transform: uppercase;
	color: #2C3E50;
	margin-bottom: 10px;
}
.fs-subtitle {
	font-weight: normal;
	font-size: 13px;
	color: #666;
	margin-bottom: 20px;
   margin-top:10px;
}
/*progressbar*/
#progressbar {
	margin-bottom: 30px;
	overflow: hidden;
   padding-left:0;
	/*CSS counters to number the steps*/
	counter-reset: step;
}
#progressbar li {
	list-style-type: none;
	color: white;
	text-transform: uppercase;
	font-size: 9px;
	width: 20%;
	float: left;
   z-index: 2;
	position: relative;
}
#progressbar li:before {
	content: counter(step);
	counter-increment: step;
	width: 20px;
	line-height: 20px;
	display: block;
	font-size: 10px;
	color: #333;
	background: white;
	border-radius: 3px;
	margin: 0 auto 5px auto;
}
/*progressbar connectors*/
#progressbar li:after {
	content: '';
	width: 100%;
	height: 2px;
	background: white;
	position: absolute;
	left: -40%;
	top: 9px;
	z-index: -1; /*put it behind the numbers*/
}
#progressbar li:first-child:after {
	/*connector not needed before the first step*/
	content: none;
}
/*marking active/completed steps green*/
/*The number of the step and the connector before it = green*/
#progressbar li.active:before,  #progressbar li.active:after{
	background: #27AE60;
	color: white;
}

</style>

      </div>
   </div>

@endsection
