@extends('layouts.page')
@section('content')
<div class="main-content">
   <div class="container">

	   <!-- start: MAIN TABLE PANEL -->
      <div class="panel panel-white" style="margin-top:8px;">

		  <!-- start: TABLE HEADER -->
		  <div class="panel-heading header_t1">

			  <div class="toolbar row" style="border: none;background: whitesmoke;min-height: 100px">

				  <div class="col-sm-6 hidden-xs">

					  <div class="table-header">
						  <h2 style="font-weight: lighter">{{ $title }}</h2>
						  <p style="font-size: large;color: silver">Lista dos seus contatos</p>
					  </div>

				  </div>

				  <div class="col-sm-6 col-xs-12">

					  <div class="toolbar-tools pull-right" style="padding-top: 10px">
						  <!-- start: TOP NAVIGATION MENU -->
						  <ul class="nav navbar-right" style="opacity: 0.7">
							  <li>
								  <a href="{{ URL::route('contacts.create') }}">
									  <i class="fa fa-user"></i> Novo Contato
								  </a>
							  </li>
							  <li>
								  <a href="#" class="print"  data-id="mainInfo">
									  <i class="fa fa-print"></i> Imprimir
								  </a>
							  </li>
						  </ul>
						  <!-- end: TOP NAVIGATION MENU -->
					  </div>

				  </div>

			  </div>

		  </div>
		  <!-- end: TABLE HEADER -->

		  <!-- start: TABLE BODY -->
         <div class="panel-body">
			 <div class="table-responsive">
            <!-- start: CONTACT TABLE DATA -->
            <table class="table datatable table-striped table-hover" id="mainInfo">
         		<thead>
         			<tr>
         				<th class="hide">  </th>
         				<th>Contato</th>
         				<th>Tipo</th>
         				<th>Telefone 1</th>
         				<th>Telefone 2</th>
         				<th></th>
         			</tr>
               </thead>
               <tbody>
                  <?php
                     if(!empty($contacts)){
                        foreach($contacts as $data){
                  ?>
            			<tr>
            				<td class="hide">{{ $data->id }}</td>
            				<td>{{ $data->title }}</td>
            				<td>
								<span class="label label-default" style="background: #0a91ff !important;opacity: 0.8">  {{ $data->contact_type }} </span>
							</td>
            				<td>{{ $data->phone1 }}</td>
            				<td>{{ $data->phone2 }}</td>
            				<td>

								<div class="btn-group hidden-print">
									<button type="button" class="btn btn-white btn-sm btn-squared dropdown-toggle" style="background: white;opacity: 0.9" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Options &nbsp;<span class="caret"></span>
									</button>
									<ul class="dropdown-menu pull-right" style="opacity:0.9;">
										<li><a href="{{ URL::route('contacts.edit', $data->id) }}"><small><i class="fa fa-pencil fa-fw"></i>&nbsp; Editar</small></a></li>
										<li class="divider"></li>
										<li><a href="#" class="deleteContact" data-id="{{$data->id}}"><small><i class="fa fa-ban fa-fw"></i>&nbsp Remover</small></a></li>
									</ul>
								</div>
                        </td>
            			</tr>
                  <?php }} ?>
               </tbody>
            </table>
            <!-- start: CONTACT TABLE DATA -->
         </div>
		  <!-- end: TABLE BODY -->

      </div>
	   <!-- start: MAIN TABLE PANEL -->

   </div>
</div>

@endsection
