@extends('layouts.page')
@section('content')
<div class="main-content">
   <div class="container">

      <!-- start: TOOLBAR -->
      <div class="toolbar row">
   		<div class="col-sm-6 hidden-xs">
   			<div class="page-header">
   				<h1>{{ $title }} <small>{{ $subtitle }}</small></h1>
   			</div>
   		</div>
   		<div class="col-sm-6 col-xs-12">
   			<div class="toolbar-tools pull-right">
   				<!-- start: TOP NAVIGATION MENU -->
   				<ul class="nav navbar-right">
   					<li>
   						<a href="{{ url('/contacts')}}" class="new-event MyToolbar">
   							<i class="fa fa-users"></i> Contatos
   						</a>
   					</li>
   				</ul>
   				<!-- end: TOP NAVIGATION MENU -->
   			</div>
   		</div>
   	</div>
      <!-- start: TOOLBAR -->

	   <!-- start: MAIN INFO PANEL -->
      <div class="panel panel-white" style="margin-top:8px;">
        <!-- <div class="panel-heading">
            <h2 class="panel-title"><i class="fa fa-user"></i> {{ $title }}</h2>
         </div> -->
			<!-- start: CONTACT INFORMATION PANEL -->
         <div class="panel-body">
            {{ Form::model($contact, ['route' => ['contacts.update', $contact->id], 'method' => 'PUT','id' => 'updateContact']) }}

               <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
      					<div class="form-group">
      					  <label for="title">Nome</label>
      					  <input class="form-control" id="title" name="title" type="text" placeholder="Nome do Contato ou Fornecedor" value="{{ $contact->title }}">
      					</div>
                     <div class="form-group">
      					  <label for="quanity">Tipo</label>
      					  <input class="form-control" id="type" name="contact_type" type="text" placeholder="Tipo de Contato"  value="{{ $contact->contact_type }}">
      					</div>
                     <div class="form-group">
      					  <label for="min_stock">Telefone 1</label>
      					  <input class="form-control" id="telephone_1" name="phone1" type="text" placeholder="Telefone 1"  value="{{ $contact->phone1 }}">
      					</div>
                     <div class="form-group">
      					  <label for="min_stock">Telefone 2</label>
      					  <input class="form-control" id="telephone_2" name="phone2" type="text" placeholder="Telefone 2" value="{{ $contact->phone2 }}" >
      					</div>
                     <div class="form-group">
                        <button class="btn btn-success btn-submit" data-loading-text="Saving..." type="submit">Atualizar Contato</button>
      					</div>
      				</div>
      			</div>
            </form>

         </div>
			<!-- end: CONTACT INFORMATION PANEL -->
      </div>
	   <!-- end: MAIN INFO PANEL -->
   </div>
</div>
@endsection
