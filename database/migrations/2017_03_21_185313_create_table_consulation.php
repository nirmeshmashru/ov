<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableConsulation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consultations',function (Blueprint $table){
            $table->increments('id');
            $table->integer('patient_id');
            $table->foreign('patient_id')->references('id')->on('patients')->onDelete('cascade');
            $table->integer('specialities_id');
            $table->foreign('specialities_id')->references('id')->on('specialities')->onDelete('cascade');
            $table->integer('dentist_id');
            $table->foreign('dentist_id')->references('id')->on('dentists')->onDelete('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('consultations');
    }
}
